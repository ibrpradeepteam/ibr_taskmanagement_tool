var $filequeue,
				$filelist;

			$(document).ready(function() {
				$filequeue = $(".demo .filelist.queue");
				$filelist = $(".demo .filelist.complete");

				$(".demo .dropped").dropper({
					action: base_url+'/upload_file',
					maxSize: 1048576
				}).on("start.dropper", onStart)
				  .on("complete.dropper", onComplete)
				  .on("fileStart.dropper", onFileStart)
				  .on("fileProgress.dropper", onFileProgress)
				  .on("fileComplete.dropper", onFileComplete)
				  .on("fileError.dropper", onFileError);

				$(window).one("pronto.load", function() {
					$(".demo .dropped").dropper("destroy").off(".dropper");
				});
			});

			function onStart(e, files) {
				//console.log("Start");

				var html = '';

				for (var i = 0; i < files.length; i++) {
					html += '<li data-index="' + files[i].index + '"><span class="file">' + files[i].name + '</span><span class="progress">Queued</span></li>';
				}

				$filequeue.append(html);
			}

			function onComplete(e) {
				//console.log("Complete");
				// All done!
			}

			function onFileStart(e, file) {
				//console.log("File Start");

				$filequeue.find("li[data-index=" + file.index + "]")
						  .find(".progress").text("0%");
			}

			function onFileProgress(e, file, percent) {
				//console.log("File Progress");

				$filequeue.find("li[data-index=" + file.index + "]")
						  .find(".progress").text(percent + "%");
			}

			function onFileComplete(e, file, response) {
				//console.log("File Complete");
				if(response.length > 0){ //Updated by Ashvin Patel 20/Mar/2015
					var file = $.parseJSON(response);
					var file_h_html = '<input type="hidden" value="'+file['id']+'" name="attach_file[]" class="attach_file file_id-'+file['id']+'">';
					  var file_s_html = '<li id="file_id-'+file['id']+'"><label>'+file['name']+'</label> <span class="delete_file pull-right" data-file-id="'+file['id']+'" title="Remove file"><i class="fa fa-times"></i></span></li>';
					$('.hidden_file_attach').append(file_h_html);	
					$('.attach_files ul').append(file_s_html);	
				}
				//console.log(response);
				/*if (response.trim() === "" || response.toLowerCase().indexOf("error") > -1) {
					$filequeue.find("li[data-index=" + file.index + "]").addClass("error")
							  .find(".progress").text(response.trim());
				} else {
					var $target = $filequeue.find("li[data-index=" + file.index + "]");

					$target.find(".file").text(file.name);
					$target.find(".progress").remove();
					$target.appendTo($filelist);
				}*/
			}

			function onFileError(e, file, error) {
				console.log("File Error");

				$filequeue.find("li[data-index=" + file.index + "]").addClass("error")
						  .find(".progress").text("Error: " + error);
			}