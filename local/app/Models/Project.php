<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Project extends Model {

	protected $fillable = array('name', 'description', 'status', 'created_by_id','tag','link','project_file','developer','manager');
    protected $table = 'project';

}