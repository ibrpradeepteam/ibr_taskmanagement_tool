<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Task extends Model {

	protected $fillable = array('title', 'status', 'assignee', 'project', 'created_by_id');
    protected $table = 'task';

}