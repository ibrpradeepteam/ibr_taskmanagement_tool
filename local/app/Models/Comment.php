<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Comment extends Model {

	protected $fillable = array('task_id', 'name', 'email', 'msg', 'created_by_id');
    protected $table = 'chat';

}