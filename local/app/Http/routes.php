<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');

Route::get('/', 'TaskController@index');

Route::get('project', 'ProjectController@index');

Route::post('/add_task', 'TaskController@add_task');

Route::post('/edit_task', 'TaskController@edit_task');

Route::post('/change_status', 'TaskController@change_task_status');

Route::post('/change_etime', 'TaskController@change_etime');

Route::post('/assign_user', 'TaskController@assign_user');

//Adavance Search
Route::post('/search_task', 'TaskController@search_task');

//Keyword Search
Route::post('/keyword_search', 'TaskController@keyword_search');

Route::post('/assign_project', 'TaskController@assign_project');

Route::get('/add_project', 'ProjectController@add_project');

Route::post('/add_project', 'ProjectController@add_project');

Route::get('/edit_project', 'ProjectController@edit_project');

Route::post('/edit_project', 'ProjectController@edit_project');

Route::get('/delete_project', 'ProjectController@delete_project');

Route::get('/sendmail', 'TaskController@sendmail');

Route::post('/add_comment', 'TaskController@add_comment');

Route::post('/get_task_comment', 'TaskController@get_task_comment');

// route to show the login form
Route::get('auth/login', array('uses' => 'HomeController@index'));

// route to process the form
Route::post('/login', array('uses' => 'HomeController@login'));

// route to logout method
Route::get('/logout', array('uses' => 'HomeController@logout'));

//Task Sortable
Route::post('/task_sortable', array('uses' => 'TaskController@task_sortable'));

//Upload file
Route::post('/upload_file', array('uses' => 'TaskController@upload_file'));

//Get Task Latest Comment
Route::post('/get_task_latest_comment', array('uses' => 'TaskController@get_task_latest_comment'));

//Delete Attachments of task
Route::post('/delete_attach', array('uses' => 'TaskController@delete_attach'));

//Delete tasks
Route::post('/delete_tasks', array('uses' => 'TaskController@delete_tasks')); 

//Track task time
Route::post('/task_time_track', array('uses' => 'TaskController@task_time_track')); 



/*Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);*/
