@include('includes.header')
@include('includes.left-sidebar')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Task Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->                   
    <!-- top row -->
    <div class="row">
        <div class="col-xs-12 connectedSortable">
            
        </div><!-- /.col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <div class="row">                       
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-8 col-sm-offset-2" style="min-height:600px;">
            <!-- Map box -->
            <div class="box box-primary" style="min-height:600px;">
            	<div class="row ad_search_box">                	
                    <div class="ad_search_wrap pull-left" style="display:none;">
                        <div class="col-md-12">
                            <label>Advance Search</label>
                        </div>
                        <div class="col-md-12">
                            <select class="selectpicker" data-width="145px" id="ads_status">
                              <option value="">Status</option>                            
                              <option>Complete</option>
                              <option>In-Progress</option>
                              <option>New</option>
                              <option>Wait for response</option>                            
                            </select>
                            <select class="selectpicker" data-width="145px" id="ads_assignee">
                              <option value="">Assignee</option>  
                              @foreach($users as $user)                          
                              <option value="{{$user['id']}}">{{$user['name']}}</option>
                              @endforeach                                                  
                            </select>
                            <select class="selectpicker" data-width="145px" id="ads_project">
                                <option value="">Project</option>
                                @foreach($projects as $project)
                                <option value="{{$project['id']}}">{{$project['name']}}</option>
                                @endforeach
                            </select>
                            <button class="btn btn-primary ad_search_btn">Go</button>                        
                        </div>
                    </div>
                </div>
                <div class="box-header">
                    <!-- tools box -->
                    <div class="box-tools">
                    <a href="javascript:void(0);" class="btn btn-info ads_sh" title="show/hide advance search"><i class="fa fa-plus"></i></a>
                    	<!--<input type="checkbox" name="check_all" class="check_all">-->  
                        <div class="col-md-2 keyword_search">
                        	<input type="text" name="search" id="search" placeholder="Search" class="input-sm form-control">                        </div>
                        <select class="selectpicker" data-width="145px" id="change_status">
                          <option value="">Change Status</option>                            
                          <option>Complete</option>
                          <option>In-Progress</option>
                          <option>New</option>
                          <option>Wait for response</option>                            
                        </select>  
                        <select class="selectpicker" data-width="145px" id="change_assignee">
                          <option value="">Assign user</option>  
                          @foreach($users as $user)                          
                          <option value="{{$user['id']}}">{{$user['name']}}</option>
                          @endforeach                                                  
                        </select>  
                        <select class="selectpicker" data-width="145px" id="change_project">
                            <option value="">Assign Project</option>
                            @foreach($projects as $project)
                            <option value="{{$project['id']}}">{{$project['name']}}</option>
                            @endforeach
                        </select>                        
                    </div><!-- /. tools -->
                    <script>
					
					</script>                    
                </div>
               
                <div class="box-body task_container" style="min-height:380px;">
                    @foreach ($tasks as $task)
                    <div class="col-md-12 task_row_wrap bottom45" id="task-row-{{$task['id']}}" data-id="{{$task['id']}}">
                        <div class="row task_row">
                        	<div class="task_wrape_scroll">
                                <div class="row">
                                    <div class="col-md-5">
                                    <input type="checkbox" name="task_select[]" class="task_select" value="{{$task['id']}}">
                                    <span class="pill task_status" id="task_status-{{$task['id']}}" style="  background-color: #FFFF00;color: #000000;">{{$task['status']}}</span>
                                    </div>
                                    <div class="task_sortable col-md-1 pull-left text-center">
                                    	<i class="fa fa-bars"></i>
                                    </div>
                                    <div class="col-md-3 pull-right text-right project-title" id="project_title-{{$task['id']}}">
                                        <label>{{$task['project_name']}}</label>
                                    </div>                                    
                                </div>
                                <div class="well edit_task" title="double click to edit">
                                    <span class="task_title">{{$task['title']}}</span>
                                    <textarea class="form-control edit_task" rows="2" id="" data-id="{{$task['id']}}" style="display:none">{{$task['title']}}</textarea>
                                    <div class="task_attachments">                                    	
                                        <ul>                                     
                                        @foreach($task['files'] as $file)
                                        	<li><a href="{{url()}}/uploads/{{$file['name']}}" download>{{$file['name']}}</a></li>
                                        @endforeach
                                        </ul>
                                    </div>
                                </div>                                            
                                <div class="row task_detail">
                                    <div class="col-md-12">
                                        <div class="col-md-3 task_time">
                                            <label>{{$task['created_by']}}<time>{{@date('dM h:i a', @strtotime($task['created_at']))}}</time></label>
                                        </div>
                                        <div class="col-md-4 estimated_time">
                                            <label class="pull-left">Estimated time: 
                                                @if(!$task['estimated_time'])
                                                    <i class="fa fa-pencil edit_e_time" title="Click to edit"></i> 
                                                @endif
                                           </label>
                                           @if(!$task['estimated_time'])
                                           <span class="col-md-4 pull-left" style="display:none;">
                                            <input type="text" name="estimat_time" class="form-control input-sm" maxlength="4" data-id="{{$task['id']}}">
                                           </span>
                                          @else
                                          <span class="col-md-4 pull-left"><span class="e_time">{{$task['estimated_time']}}</span></span>
                                          @endif                                    
                                        </div>
                                        <div class="col-md-4 pull-right">
                                            <label>Assignee:</label> <span class="task_assignee" id="task_assignee-{{$task['id']}}">@if($task['assignee']) {{$user_m->get_username($task['assignee'])}}  @endif</span>
                                        </div>
                                   </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 threads_wrap top5">
                                        <div class="user_threads">
                                            <ul>
                                               <!-- <li class="adminComment">
                                                    <div class="comment">
                                                        <p><a href="#">Admin</a> </p>
                                                        <p>You can just rename the </p>
                                                        <span class="comment_time"></span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="comment">
                                                        <p><a href="#">You</a> </p>
                                                        <p>You can just rename the </p>
                                                        <span class="comment_time"></span>
                                                    </div>
                                                </li>-->
                                            </ul>
                                        </div>
                                        <div class="thread_input top10 bottom5">
                                            <textarea class="form-control add_comment" rows="2" id="add_comment-{{$task['id']}}" data-task-id="{{$task['id']}}"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span class="task_expander expand" data-task-id="{{$task['id']}}">
                                <i class="fa pull-right fa-angle-double-down"></i>
                                <i class="fa pull-right fa-angle-double-up"></i>
                            </span>
                        </div>
                        
                    </div>
                    @endforeach
                    <!--<div class="demo" style="float: left;  width: 100%;">
                        <form action="#" class="demo_form">
                            <div class="dropped"></div> 
                        </form>
                    </div>-->
                </div>
                
            </div>           
<!-- /.box -->    
</section><!-- /.content -->                     
@include('includes.footer')
<style>
  .demo .filelists { margin: 20px 0; }
  .demo .filelists h5 { margin: 10px 0 0; }

  .demo .filelist { margin: 0; padding: 10px 0; }
  .demo .filelist li { background: #fff; border-bottom: 1px solid #eee; font-size: 14px; list-style: none; padding: 5px; }
  .demo .filelist li:before { display: none; }
  .demo .filelist li .file { color: #333; }
  .demo .filelist li .progress { color: #666; float: right; font-size: 10px; text-transform: uppercase; }
  .demo .filelist li .delete { color: red; cursor: pointer; float: right; font-size: 10px; text-transform: uppercase; }
  .demo .filelist li.complete .progress { color: green; }
  .demo .filelist li.error .progress { color: red; }
</style>

 <div class="box-footer demo">
 	<div class="attach_files">
    	<ul>
        
        </ul>
    </div>
 <form action="#" class="demo_form"> 					
                  <!--<textarea class="form-control add_task " rows="3" id="add_task"></textarea>-->
                    <div class="" id="add_task" >
                    	<div class="dropped"></div>                        
                    </div>
                    <div class="hidden_file_attach">
                    	
                    </div>
                    </form>
                </div>
