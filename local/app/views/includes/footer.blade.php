	</aside><!-- /.right-side -->
</div><!-- ./wrapper -->
<!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="{{url()}}/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{url()}}/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="{{url()}}/js/plugins/morris/morris.min.js" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="{{url()}}/js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="{{url()}}/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="{{url()}}/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="{{url()}}/js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="{{url()}}/js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="{{url()}}/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{url()}}/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="{{url()}}/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

		<!-- DATA TABES SCRIPT -->
        <script src="{{url()}}/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{url()}}/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        
        <!--FANCYBOX-->
        
        <script type="text/javascript" src="{{url()}}/js/jquery1.fancybox.js"></script>
        <link rel="stylesheet" type="text/css" href="{{url()}}/css/fancybox/jquery.fancybox.css" media="screen" />
        <script type="text/javascript">
        $(document).ready(function() {			
            $('.fancybox').fancybox();
        });
        </script>
        
        <!--Datepicker-->
        <script type="text/javascript" src="{{url()}}/js/bootstrap-datepicker.js"></script>
        
        <!--Chosen Multiselect-->
        <script type="text/javascript" src="{{url()}}/js/chosen.jquery.js"></script>
        
        <!-- AdminLTE App -->
        <script src="{{url()}}/js/AdminLTE/app.js" type="text/javascript"></script>
        
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{url()}}/js/AdminLTE/dashboard.js" type="text/javascript"></script>     
        
        <!-- AdminLTE for demo purposes -->
        <script src="{{url()}}/js/AdminLTE/demo.js" type="text/javascript"></script>
        
        <!--Bootstrape select box-->
        <script type="text/javascript" src="{{url()}}/js/bootstrap-select.js"></script>
        
        <!--Drag drop file upload -->        
		<script src="{{url()}}/js/jquery.fs.dropper.js"></script>
        
        <!--Wait Me Loader-->
        <script src="{{url()}}/js/waitMe.js"></script>
        
        <!--Time Tracker-->
		<script src="{{url()}}/js/jquery.cookie.js"></script>
        <script src="{{url()}}/js/jquery.stopwatch.js"></script>
        
        <!--Angular js-->
        <script src="{{url()}}/js/angular.min.js" type="text/javascript"></script>
        
		<script src="{{url()}}/js/date.js" type="text/javascript"></script>
        <script src="{{url()}}/js/script.js" type="text/javascript"></script>
        <script src="{{url()}}/js/upload.js" type="text/javascript"></script>
	</body><!--.body end-->
</html><!--.HTML end-->