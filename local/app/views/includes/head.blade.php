<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="_token" content="{{ csrf_token() }}" />
        <title>IBR Task Management | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="{{url()}}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{url()}}/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{url()}}/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="{{url()}}/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="{{url()}}/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="{{url()}}/css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="{{url()}}/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="{{url()}}/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{url()}}/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        
        <link href="{{url()}}/css/style.css" rel="stylesheet" type="text/css" />
        
        <!--DATA TABLES-->
		<link href="{{url()}}/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        
        <!--Drag drop file upload -->
        <link href="{{url()}}/css/jquery.fs.dropper.css" rel="stylesheet" type="text/css" media="all">
        
        <!--Datepicker-->
        <link href="{{url()}}/css/datepicker.css" rel="stylesheet" type="text/css" media="all">
        
        <!--Chosen Multiselect-->
        <link href="{{url()}}/css/chosen.css" rel="stylesheet" type="text/css" media="all">
		
        <!--Wait Me Loader-->
        <link type="text/css" rel="stylesheet" href="{{url()}}/css/waitMe.css">
        
        <!--Select box bootstrap-->
        <link rel="stylesheet" type="text/css" href="{{url()}}/css/bootstrap-select.css">
        <script>
		var base_url = '{{url()}}';
		var csrf_token = '{{ csrf_token() }}';
		var session_user_id = '{{Session::get("user_id")}}';
		var total_tasks  = '@if(isset($tasks)) {{count($tasks)}} @endif';
		var user_type  = '{{Session::get("user_type")}}';
		</script>
    </head>
<body class="skin-blue">