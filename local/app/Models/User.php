<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class User extends Model {

	protected $fillable = array('name', 'email');
    protected $table = 'users';
	
	public function get_username($id){			
		$users = $this->where('id', $id)->get();
		return isset($users[0]->name) ? $users[0]->name : '';	
	}
	public function get_user_detail($username='', $password=''){
		if($username&&$password){
			$result = $this->where('email', $username)->where('password', $password)->first();
			return $result;	
		}
		return '';
	}

}