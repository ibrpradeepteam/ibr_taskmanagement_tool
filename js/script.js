var last_comment_id = '';
var interval = false;
$(document).ready(function(e){
   $('body').on('click', '.expand', function(e){ 
   		var elem = $(this);
        $('.task_row_wrap').hide();
        $(this).parent().parent().show();
       // $(this).parent().parent().removeClass('top30');
        $(this).removeClass('expand');
        $(this).addClass('collapse');
        $(this).parent().addClass('exapand_row');   
		$(this).parent().find('.threads_wrap').show();	
		$('.exapand_row .task_wrape_scroll').slimScroll({
			height: '500px'
		});	
		$(this).parent().find('.task_sortable').hide();
		get_task_conversation(elem);
		interval = true;		
   });
    $('body').on('click', '.collapse', function(e){  
		$(".exapand_row .task_wrape_scroll").slimScroll({destroy: true}); 
		$(".exapand_row .task_wrape_scroll").removeAttr('style');
        $('.task_row_wrap').show();
        $(this).removeClass('collapse');
        $(this).addClass('expand');
        $(this).parent().removeClass('exapand_row');
        if($(this).parent().parent().index() > 0){
            //$(this).parent().parent().addClass('top30');
        }
		$(this).parent().find('.threads_wrap').hide();
		$(this).parent().find('.task_sortable').show();
		interval = false;	
    });
    $('.task_container').slimScroll({
        height: '600px'
    });
	
	/*$('.user_threads').slimScroll({
        height: '180px',
		start: 'bottom',
    });*/
	
	//Initialize Chosen multiselect lib
	var config = {
      '.chzn-select'           : {},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"100%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
    
    var today = Date.today().toString("dMMM ");
    today += new Date().toString("hh:mm tt");
    $('.comment_time').text(today);
	 $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });
	$('.selectpicker').selectpicker({
		style: 'btn',
	});	
	
	/*
	* Change status of selected task
 	* Ashvin patel 17/Mar/2015
	*/
	$('#change_status').change(function(e) {
        var status = $(this).val();
		var tasks = [];
		var more_task = true;
		var i = 0;
		$('.task_select').each(function(index, element) {
			var task_id = $(element).val();
			i = ($('#task_status-'+task_id).text() == 'In-Progress') ? parseInt(i)+1 : 1;
			if(i>1){
				alert('At a time only one task status should be "In-progress"');
				more_task = false;
				return;	
			}
			if($(element).is(':checked')){
            	tasks.push($(element).val());
			}
        });
		if(more_task){
			if(tasks[0]){
				$.ajax({	  
					url : base_url+'/change_status',
					type : "post",
					data: {_token:csrf_token,tasks:tasks,status:status},
					success: function(json){
						$.each(tasks, function(i, v){
							$('#task_status-'+v).text(status);	
							if(status == 'In-Progress'){
								$('#task-row-'+v).find('.time').stopwatch().stopwatch('start');
								start_timer(v);							
							}else{
								$('#task-row-'+v).find('.time').stopwatch().stopwatch('stop');	
								stop_timer(v);
							}
						});
						$('#change_status').val('');
						$('button[data-id="change_status"]').parent().find('ul li').removeClass('selected');
						$('button[data-id="change_status"]').find('span.filter-option').text('Change Status');
						$('.task_select').prop('checked',false);
						
					}
				});
			}
		}
    });
	/*
	* Change Assignee of selected task
 	* Ashvin patel 17/Mar/2015
	*/
	$('#change_assignee').change(function(e) {
        var assignee = $(this).val();
		var assignee_name = $(this).find('option:selected').text();
		var tasks = [];
		$('.task_select').each(function(index, element) {
			if($(element).is(':checked')){
            	tasks.push($(element).val());
			}
        }); 
		if(tasks[0]){
			$.ajax({	  
				url : base_url+'/assign_user',
				type : "post",
				data: {_token:csrf_token,tasks:tasks,assignee:assignee},
				success: function(json){
					$.each(tasks, function(i, v){
						$('#task_assignee-'+v).text(assignee_name);	
						if($('#task-row-'+v).find('.time').parent().find('.btn').attr('data-user-id') != assignee){
							$('#task-row-'+v).find('.time').text('00:00:00');
							$('#task-row-'+v).find('.time').stopwatch().stopwatch('toggle');
							$('#task-row-'+v).find('.time').stopwatch().stopwatch('reset');
							$('#task-row-'+v).find('.time').parent().find('.btn').removeClass('btn-danger');
							$('#task-row-'+v).find('.time').parent().find('.btn').addClass('btn-success');
							$('#task-row-'+v).find('.time').parent().find('.btn .start_stop_text').text('Start');
							$('#task-row-'+v).find('.time').parent().find('.btn').attr('data-user-id', assignee);
							$('#task-row-'+v).find('.worked_time').attr('data-worked-hours', 0);
							$('#task-row-'+v).find('.worked_time').attr('data-worked-minutes', 0);
							$('#task-row-'+v).find('.worked_time .my_label').text('0h 0m');
						}
					});
					$('#change_assignee').val('');
					$('button[data-id="change_assignee"]').parent().find('ul li').removeClass('selected');
					$('button[data-id="change_assignee"]').find('span.filter-option').text('Assign user');
					$('.task_select').prop('checked',false);
				}
			});
		}
    });
	
	/*
	* Assign project to task
 	* Ashvin patel 18/Mar/2015
	*/
	$('#change_project').change(function(e) {
        var project = $(this).val();
		var project_name = $(this).find('option:selected').text();
		var tasks = [];
		$('.task_select').each(function(index, element) {
			if($(element).is(':checked')){
            	tasks.push($(element).val());
			}
        }); 
		if(tasks[0]){
			$.ajax({	  
				url : base_url+'/assign_project',
				type : "post",
				data: {_token:csrf_token,tasks:tasks,project:project},
				success: function(json){
					$.each(tasks, function(i, v){
						$('#project_title-'+v+' label').text(project_name);	
					});
					$('#change_project').val('');
					$('button[data-id="change_project"]').parent().find('ul li').removeClass('selected');
					$('button[data-id="change_project"]').find('span.filter-option').text('Assign Project');
					$('.task_select').prop('checked',false);
				}
			});
		}
    });
	
	/*
	* Delete Selected tasks
 	* Ashvin patel 23/Mar/2015
	*/
	$('#delete_task').click(function(e) {		
		var tasks = [];
		$('.task_select').each(function(index, element) {
			if($(element).is(':checked')){
				tasks.push($(element).val());
			}
		});			
		if(tasks[0]){
			if(confirm('Are you sure want to delete the selected tasks!')){
				$.ajax({	  
					url : base_url+'/delete_tasks',
					type : "post",
					data: {_token:csrf_token,tasks:tasks},
					success: function(json){
						$.each(tasks, function(i, task_id){
							$('#task-row-'+task_id).remove();
						});
					}
				});
			}
		}else{
			alert('First Select atleast one task!')	
		}
    });
	
	$('body').on('click', '.edit_e_time', function(e) {
        $(this).parent().parent().find('span').show();
    });
	$('body').on('dblclick', '.edit_task', function(e) {		
        $(this).find('.task_title').hide();
		$(this).find('.edit_task').show();
		$(this).find('.close_edit_window').show();
		clearSelection(); 
    });	
	$('body').on('click', '.close_edit_window', function(e) {
		$(this).parent().find('.task_title').show();
		$(this).parent().find('.edit_task').hide();
		$(this).hide();
	});
	/*
	* Call Search method
	* Ashvin Patel 18/Mar/2015
	*/
	$('#search').blur(function(e) {        
		var elem = $(this);
		var keyword = (elem) ? elem.val() : '';
		run_waitMe('.task_container', 'win8');
		$('.time').stopwatch().stopwatch('reset');
		 $.ajax({	  
			url : base_url+'/keyword_search',
			type : "post",
			data: {_token:csrf_token,keyword:keyword},
			success: function(json){
				if(json.length > 0){
					var tasks = $.parseJSON(json);				
					var html  = '';
					var es_time = '';				
					$.each(tasks, function(i, v){
						html += task_html(v);						
					});
					total_tasks = tasks.length;				
					$('.total_task span').text(total_tasks);
					html = (html) ? html : '<div class="col-md-12"><div class="not_found text-center">no task found</div></div>'
					$('.task_container').html(html);
					/*$("input[type='checkbox'], input[type='radio']").iCheck({
						checkboxClass: 'icheckbox_minimal',
						radioClass: 'iradio_minimal'
					});*/
					$(".task_container").sortable('destroy');
					task_sortable();
					stop_waitMe('.task_container');
					timer_auto_start(); 
				}		
						
			}
		});	
    });
	$('.ads_sh').click(function(e) {
		var elem = $(this);
        $('.ad_search_wrap').slideToggle('slow');
		setTimeout(function(){						
			if($('.ad_search_wrap').css('display') == 'none'){
				elem.find('i').attr('class', 'fa fa-plus');
			}else{
				elem.find('i').attr('class', 'fa fa-minus');	
			}
		}, 700);
		
    });
	$('.ad_search_btn').click(function(e) {
        var status = $('#ads_status').val();
		var assignee = $('#ads_assignee').val();
		var project = $('#ads_project').val();
		var date = $('#ads_date').val();
		var data = {
			status: status,
			assignee: assignee,
			project: project,
			date:date
		}
		task_search('', data);
    });
	$('.add_project').click(function(e) {
        var url = base_url+'/add_project';
		fancybox(url, 1024, 600);
    });
	
	$('.edit_project').click(function(e) {
		var id = $(this).attr('id');
        var url = base_url+'/edit_project?id='+id;
		fancybox(url, 1024, 600);
    });
	
	$('.delete_project').click(function(e) {
		
		
		if(confirm("Are You Confirm Delete this")){
			var id = $(this).attr('id');
			var url = base_url+'/delete_project?id='+id;
		
			$.ajax({
			  url: url,
			   success:function(data){
                window.location=base_url+"/project";
               }
			});
			
			
			}
		
    });
	
	
	/*
	* remove attach file
	* 
	* Ashvin Patel 20/Mar/2015
	*/	
	$('body').on('click', '.delete_file', function(e) {
		var file_id = $(this).attr('data-file-id');
		$('#file_id-'+file_id).remove();
		$('.file_id-'+file_id).remove();
	});
	
	$('body').on('click', '.add_more_files', function(e) {
		var elem = $(this);
		var task_id = $(this).attr('data-task-id');
		var elem = $(this);
        elem.find('input[name="task_file"]')[0].click();
		
    });	
	$('body').on('change', '.task_file', function(e) {
		var elem = $(this);
		var file = $(this)[0].files[0];
		var task_id = elem.parent().attr('data-task-id');
		
		var data = new FormData();		
		data.append('_token', csrf_token);
		data.append('file', file);
		data.append('task_id', task_id); 		   
		$.ajax({
			url: base_url+'/upload_file',
			type: 'POST',					
			contentType:false,
			processData: false,
			data: data,	
			cache: false,
			success: function(response){
				if(response.length > 0){ //Updated by Ashvin Patel 21/Mar/2015
					var file = $.parseJSON(response);					
					var file_html = '<li><a href="'+base_url+'/uploads/'+file['name']+'" download>'+file['name']+'</a></li>';					
					$('#task-row-'+task_id).find('.task_attachments ul').append(file_html);	
				}
			}
		});
    });
	$('body').on('click', '.delete_task_file', function(){
		var confirmv = confirm('Are you sure want to delete this file!');
		if(confirmv){
			var file_id = $(this).attr('data-file-id');
			var elem = $(this);
			$.ajax({
				url: base_url+'/delete_attach',
				type: 'POST',		
				data: {_token:csrf_token,file_id:file_id},			
				success: function(response){
					elem.parent().remove();
				}
			});	
		}
	});
	$('.show_add_task_box').click(function(e) {
		var elem = $(this);
        $('.add_task_footer_box').slideToggle('slow');
		//$('.change_assignee').toggle('slow');
		//$('.add_task_footer_box').toggle('slide', {direction:'right'}, 2000);
		setTimeout(function(){		
			//$('.change_assignee').slideToggle('slow');				
			if($('.add_task_footer_box').css('display') == 'none'){
				elem.find('i').attr('class', 'fa fa-plus');				
				var is_checked = 0;
				$('.task_select').each(function(index, element) {
					if($(element).is(':checked')){
						is_checked = parseInt(is_checked)+1;	
					}
				});
				if(is_checked > 0){
					$('.change_assignee').slideDown('slow');	
				}else{
					$('.change_assignee').slideUp('slow');		
				}
			}else{
				elem.find('i').attr('class', 'fa fa-minus');				
				var is_checked = 0;
				$('.task_select').each(function(index, element) {
					if($(element).is(':checked')){
						is_checked = parseInt(is_checked)+1;	
					}
				});
				if(is_checked > 0){
					$('.change_assignee').slideDown('slow');	
				}else{
					$('.change_assignee').slideDown('slow');		
				}
			}
		}, 700);
    });
	$('body').on('click', '.task_select', function(e) {		
		var is_checked = 0;
        $('.task_select').each(function(index, element) {
            if($(element).is(':checked')){
				is_checked = parseInt(is_checked)+1;	
			}
        });
		if(is_checked > 0){
			$('.change_assignee').slideDown();	
		}else{
			if($('.add_task_footer_box').css('display') == 'none'){
				$('.change_assignee').slideUp();
			}
		}
    });
	$('.s_h_search').click(function(e) {
		var elem = $(this);
        $('.adsearch_box').slideToggle('slow');
		setTimeout(function(){
			if($('.adsearch_box').css('display') == 'none'){
				elem.find('i').attr('class', 'fa fa-plus');				
			}else{
				elem.find('i').attr('class', 'fa fa-minus');			
			}
		}, 700);
    });
});
/*
* Search
* Ashvin Patel 18/Mar/2015
*/
function task_search(elem, data){
	var keyword = (elem) ? elem.val() : '';
	run_waitMe('.task_container', 'win8');
	reset_timer();
	 $.ajax({	  
		url : base_url+'/search_task',
		type : "post",
		data: {_token:csrf_token,keyword:keyword,data:data},
		success: function(json){
			if(json.length > 0){
				var tasks = $.parseJSON(json);				
				var html  = '';
				var es_time = '';				
				$.each(tasks, function(i, v){
					html += task_html(v);						
				});
				total_tasks = tasks.length;				
				$('.total_task span').text(total_tasks);
				html = (html) ? html : '<div class="col-md-12"><div class="not_found text-center">no task found</div></div>'
				$('.task_container').html(html);
				/*$("input[type='checkbox'], input[type='radio']").iCheck({
					checkboxClass: 'icheckbox_minimal',
					radioClass: 'iradio_minimal'
				});*/
				$(".task_container").sortable('destroy');
				task_sortable();
				stop_waitMe('.task_container');
				timer_auto_start(); 				
			}		
					
		}
	});	
}
function reset_timer(){
	$('.time').each(function(index, element) {
        $(this).stopwatch().stopwatch('reset');	
    });
}
function task_html(task, files){
	 
	if(task['estimated_time']){
		 es_time  = '<label title="Click to edit" class="pull-left">Estimated time:</label><span class="col-md-4 pull-left"><span class="e_time">'+task['estimated_time']+'</span></span>';  
	}else{
		 es_time  = '<label title="Click to edit" class="pull-left">Estimated time: <i class="fa fa-pencil edit_e_time"></i></label><span class="col-md-4 pull-left" style="display:none;"><input type="text" name="estimat_time" class="form-control input-sm" maxlength="10" data-id="'+task['id']+'"></span>';	
	}
	var files_html = '';
	if(task['files']){		
		$.each(task['files'], function(index, file){
			files_html += '<li id="task_file_'+file['id']+'"><a href="'+base_url+'/uploads/'+file['name']+'" download>'+file['name']+'</a> <a href="javascript:void(0);" class="delete_task_file" data-file-id="'+file['id']+'"><i class="fa fa-times"></i></a></li>';
		});	
	}
	var assignee  = (task['assignee_name']) ? task['assignee_name'] : '';
	var project  = (task['project_name']) ? task['project_name'] : '';
	var created_at = new Date(task['created_at']);
	var created_at_1 = created_at.toString("dMMM ");
	 created_at_1 += created_at.toString("hh:mm tt");
	var task_title =  task['title'].replace( new RegExp('\n', 'g'), '<br>' );
	var hours = (task['worked_time']) ? Math.floor(task['worked_time']/60) : 0;
	var minutes = (task['worked_time']) ? task['worked_time']%60 : 0;
	var html = '<div class="col-md-12 task_row_wrap bottom45" id="task-row-'+task['id']+'" data-id="'+task['id']+'">\
		<div class="row task_row">\
			<div class="task_wrape_scroll">\
			<div class="row">\
				<div class="col-md-4">\
				<input type="checkbox" name="task_select[]" class="task_select" value="'+task['id']+'">\
				<span class="pill task_status" id="task_status-'+task['id']+'" style="  background-color: #FFFF00;color: #000000;">'+task['status']+'</span>\
				</div>\
				<div class="task_sortable col-md-1 pull-left text-center">\
					<i class="fa fa-bars"></i>\
				</div>\
				<div class="col-md-3 pull-right text-right project-title" id="project_title-'+task['id']+'">\
					<label>'+project+'</label>\
				</div>\
				<div class="my_timer col-md-3 pull-right"><div class="col-md-6 worked_time" data-worked-hours="'+hours+'" data-worked-minutes="'+minutes+'"><span class="my_label">'+hours+'h '+minutes+'m</span>\
				  </div>\
				  <div class="time">00:00:00</div>\
				  <button class="btn btn-success" type="submit" data-task-id="'+task['id']+'" data-user-id="'+task['assignee']+'"><i class="icon-white icon-play"></i><span class="start_stop_text">Start</span></button></div>\
			</div>\
			<div class="well edit_task" title="double click to edit">\
				<a href="javascript:void(0);" class="close_edit_window"><i class="fa fa-times"></i></a><span class="task_title">'+task_title+'</span>\
				<img src="'+base_url+'/img/6.gif">\
				 <textarea class="form-control edit_task" rows="3" data-id="'+task['id']+'" style="display:none">'+task['title']+'</textarea>				 <div class="task_attachments"><ul>\
					'+files_html+'\
					</ul>\
				</div>\
				<div class="col-md-2 add_more_files" title="Add more files" data-task-id="'+task['id']+'">\
					<i class="fa fa-upload"></i>\
					<input type="file" name="task_file" id="task_file_'+task['id']+'" class="task_file" style="display:none;">\
				</div>\
				</div>\
			<div class="row task_detail">\
				<div class="col-md-12">\
					<div class="col-md-3 task_time">\
						<label>'+$.ucfirst(task['created_by'])+' <time>'+created_at_1+'</time></label>\
					</div>\
					<div class="col-md-4 estimated_time">\
					   '+es_time+'\
					</div>\
					<div class="col-md-4 pull-right">\
						<label>Assignee:</label> <span class="task_assignee" id="task_assignee-'+task['id']+'">'+assignee+'</span>\
					</div>\
			   </div>\
			</div>\
			<div class="row">\
				<div class="col-md-12 threads_wrap top5">\
					<div class="user_threads">\
						<ul></ul>\
					</div>\
					<div class="thread_input top10 bottom5">\
						<textarea class="form-control add_comment" rows="2" id="add_comment-'+task['id']+'" data-task-id="'+task['id']+'"></textarea>\
					</div>\
				</div>\
			</div>\
			</div>\
			<span class="task_expander expand" data-task-id="'+task['id']+'">\
				<i class="fa pull-right fa-angle-double-down"></i>\
				<i class="fa pull-right fa-angle-double-up"></i>\
			</span>\
		</div>\
	</div>';	
	return html;
}
function clearSelection() {
	var sel ;
	if(document.selection && document.selection.empty){
		document.selection.empty() ;
	} else if(window.getSelection) {
		sel=window.getSelection();
		if(sel && sel.removeAllRanges)
			sel.removeAllRanges() ;
	}
}
function add_task(value, attach_files, data){	
	var value =  value.replace( new RegExp('<br>', 'g'), '\n' );
   $.ajax({	  
		url : base_url+'/add_task',
		type : "post",
		data: {_token:csrf_token,task:value,attach_files:attach_files,data:data},
		success: function(json){
			if(json.length > 0){
				var task = $.parseJSON(json);			   
				var html = task_html(task);
				$('.task_container').prepend(html);	
				/*$("input[type='checkbox'], input[type='radio']").iCheck({
					checkboxClass: 'icheckbox_minimal',
					radioClass: 'iradio_minimal'
				});*/
				$(".task_container").sortable('destroy');
				task_sortable();
				total_tasks = parseInt(total_tasks)+1;
				$('.total_task span').text(total_tasks);
			}
		}
	});	
}
function edit_task(elem, value, task_id){
   $.ajax({	  
		url : base_url+'/edit_task',
		type : "post",
		data: {_token:csrf_token,task:value,task_id:task_id},
		success: function(json){			
		  elem.hide();
		  var task_title =  value.replace( new RegExp('\n', 'g'), '<br>' );
		  elem.parent().find('.task_title').html(task_title);
		  elem.parent().find('.task_title').show();
		  elem.val(value);	
		  elem.parent().find('img').hide();			
		}
	});	
}
$(window).load(function(){	
	$('.add_task').keyup(function(e){		
		if(e.keyCode == 13 && !e.shiftKey){ 
			if($(this).html()){	
				var attach_file = [];	
				var task_data = {'assignee': $('#change_assignee').val(),'project':$('#change_project').val()};
				$('.attach_file').each(function(index, element) {
                    attach_file.push($(element).val());
                });
				//attach_file = attach_file.join(",");				
				add_task($(this).html(), attach_file, task_data);  
				$(this).text('');
				$('.hidden_file_attach').html('');
				$('.attach_files ul').html('');
			}
		}
	});
	$('body').on('keyup', '.edit_task', function(e){
		if(e.keyCode == 13 && !e.shiftKey){ 
			if($(this).val()){	
			var elem = $(this);	
				edit_task(elem, $(this).val(), $(this).attr('data-id'));  
				$(this).hide();
				elem.parent().find('.close_edit_window').hide();
				elem.parent().find('img').show();
			}
		}
	});
    /* 
     * Disable new line on enter key in textarea
     * Ashvin Patel 14/Mar/2015
     * */
	$('body').on('keydown', "#add_task, .add_comment, .edit_task", function(e){
        if (e.keyCode == 13 && !e.shiftKey){
          // prevent default behavior
          e.preventDefault();
          //alert("ok");
          return false;
        }
    });
	$('body').on('blur', 'input[name="estimat_time"]', function(e){
		//if(e.keyCode == 13){ 
			
			if($(this).val()){		
				var task_id = $(this).attr('data-id');
				var time = $(this).val();
				var elem = $(this);
				$.ajax({	  
					url : base_url+'/change_etime',
					type : "post",
					data: {_token:csrf_token,task_id:task_id,time:time},
					success: function(json){
						elem.parent().parent().find('.edit_e_time').remove();
						elem.parent().html('<span class="e_time">'+time+'</span>');									
					}
				});
			}else{
				$(this).parent().hide();	
			}
		//}
	});
	
	/*
	* Add conversation 
	* Ashvin Patel 19/Mar/2015
	*/
	$('body').on('keyup', '.add_comment', function(e){
		if(e.keyCode == 13 && !e.shiftKey){ 
			if($(this).val()){	
				var elem = $(this);	
				add_comment(elem, $(this).val(), $(this).attr('data-task-id'));  
				elem.val('');
			}
		}
	});
});
function comment_html(data){
	var html = '';
	var commenter_name = (data['created_by_id'] == session_user_id) ? 'You' : data['name'];
	var comment_class = (data['created_by_id'] == session_user_id) ? '' : 'adminComment';
	html = '<li class="'+comment_class+'" id="comment_row-'+data['id']+'">\
				  <div class="comment">\
					  <p><a href="#">'+$.ucfirst(commenter_name)+'</a> </p>\
					  <p>'+nl2br(data['msg'])+'</p>\
					  <span class="comment_time">'+get_parsetime(data['created_at'])+'</span>\
				  </div>\
			   </li>';
	return html;	
}
function add_comment(elem, val, task_id){
	$.ajax({	  
	  url : base_url+'/add_comment',
	  type : "post",
	  data: {_token:csrf_token,task_id:task_id,msg:val},
	  success: function(json){
		if(json.length > 0){
			var comment = $.parseJSON(json);
			var html = comment_html(comment);
			//console.log(elem.parent().parent().find('.user_threads ul').find('comment_row-'+comment['id']).length);
			if(elem.parent().parent().find('.user_threads ul').find('#comment_row-'+comment['id']).length < 1){
				elem.parent().parent().find('.user_threads ul').append(html);
				last_comment_id = comment['id'];			
				var last_comment_height = elem.parent().parent().find('.user_threads ul li:last-child .comment').css('height');			
				last_comment_height = last_comment_height.replace( new RegExp('px', 'g'), '' );
				last_comment_height = parseInt(last_comment_height) + 20;			
				var scroll_div = elem.parent().parent().find('.user_threads');
				$(scroll_div).slimScroll({scrollBy: last_comment_height+'px'});	
			}
		}
	  }
  });	
}

function fancybox(url, width, height){
  $.fancybox({
	type: 'iframe',
	href: url,
	autoSize: false,
	closeBtn: true,
	width: width,
	height: height,
	closeClick: true,
	enableEscapeButton: true,
	beforeLoad: function () {},
  });
}

/*
* parse Time
* ashvin Patel 19/Mar/2015
*/
function get_parsetime(time){
	if(time){
		var par_time = new Date(time);
		var formated_time = par_time.toString("dMMM ");
		formated_time += par_time.toString("hh:mm tt");	
		return formated_time;
	}
}
function get_task_conversation(elem){
	var task_id = elem.attr('data-task-id');
	$.ajax({	  
	  url : base_url+'/get_task_comment',
	  type : "post",
	  data: {_token:csrf_token,task_id:task_id},
	  success: function(json){
		if(json.length > 0){
			var comments = $.parseJSON(json);
			var html = '';
			$.each(comments, function(i, comment){
				html += comment_html(comment);
				last_comment_id = comment['id'];					
			});
			elem.parent().parent().find('.user_threads ul').html(html);
			var scroll_div = elem.parent().parent().find('.user_threads');
			$(scroll_div).slimScroll({  height: '104%',start: 'bottom'});			
		}
		get_latest_comment(task_id);
	  }
  });	
}
task_sortable();
function task_sortable(){
  $(".task_container").sortable({
	  // Add a handle otherwise the mouse and
	  // selection are interfering with the editables 
	  handle: ".task_sortable >i",
	  connectWith : ".task_container",
	  update: function() {
		  var tasks = [];
		  $('.task_row_wrap').each(function(index, elem) {
			  tasks.push($(elem).attr('data-id'));
		  });			  
		  if(tasks.length > 0){
			  $.ajax({	  
				  url : base_url+'/task_sortable',
				  type : "post",
				  data: {_token:csrf_token,tasks:tasks},
				  success: function(json){
													  
				  }
			  });
		  }
	  },		
  });
  // Don't user disableSelection();
  //$( ".task_container" ).disableSelection();
}

function get_latest_comment(task_id){	
	var task_id = task_id;
	$.ajax({	  
	  url : base_url+'/get_task_latest_comment',
	  type : "post",
	  data: {_token:csrf_token,task_id:task_id,last_comment_id:last_comment_id},
	  success: function(json){
		if(json.length > 0){
			var comments = $.parseJSON(json);
			var html = '';
			$.each(comments, function(i, comment){
				if($('#task-row-'+task_id).find('.user_threads ul').find('#comment_row-'+comment['id']).length < 1){
					html += comment_html(comment);
					last_comment_id = comment['id'];
				}
			});
			$('#task-row-'+task_id).find('.user_threads ul').append(html);
			var scroll_div = $('#task-row-'+task_id).find('.user_threads');
			var  last_comment_height = $('#task-row-'+task_id).find('.user_threads').height();			
			$(scroll_div).slimScroll({scrollBy: last_comment_height+'px'});					
		}
	  }
	});
	setTimeout(function(){
		if(interval){
			get_latest_comment(task_id);
		}
	}, 4000);	
}
function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

/*
* Convert first charcter of string to uppercase
* Ashvin Patel 21/Mar/2015 
*/
(function($) {
    $.ucfirst = function(str) {
        /*var text = str;
        var parts = text.split(' '),
            len = parts.length,
            i, words = [];
        for (i = 0; i < len; i++) {
            var part = parts[i];
            var first = part[0].toUpperCase();
            var rest = part.substring(1, part.length);
            var word = first + rest;
            words.push(word);
        }*/
        return str;
    };

})(jQuery);

/*
* Initialize Loader
* Ashvin patel 23/Mar/2015
*/
function run_waitMe(container, effect){
  $(container).waitMe({
	  effect: effect,
	  text: 'Please wait...',
	  bg: 'rgba(255,255,255,0.7)',
	  color:'#000',
	  sizeW:'',
	  sizeH:'',
	  source: 'img.svg'
  });
}
function stop_waitMe(container){
	$(container).waitMe('hide');	
}
function start_timer(v){
	var locateAllButtons = $('.my_timer .btn');
	locateAllButtons.addClass('disabled');	
	$('#task-row-'+v).find('.time').parent().find('.btn').removeClass('disabled');
	$('#task-row-'+v).find('.time').parent().find('.btn').addClass('btn-danger');
	$('#task-row-'+v).find('.time').parent().find('.btn').removeClass('btn-success');
	$('#task-row-'+v).find('.time').parent().find('.btn .start_stop_text').text('Stop');	
}
function stop_timer(v){
	var locateAllButtons = $('.my_timer .btn');
	locateAllButtons.removeClass('disabled');
	$('#task-row-'+v).find('.time').parent().find('.btn').removeClass('btn-danger');
	$('#task-row-'+v).find('.time').parent().find('.btn').addClass('btn-success');
	$('#task-row-'+v).find('.time').parent().find('.btn .start_stop_text').text('Start');		
}
(function($){
	$("body").on('click', 'div.my_timer > .btn', function(event){
        var locateClickedElement = $(this);
        var locateAllButtons = $('.my_timer .btn');
		var task_id = locateClickedElement.attr('data-task-id');
        var tasks = [task_id];
        // no more activity is possible, 
        // only one task can be active
        if(locateClickedElement.hasClass('disabled')) return;
        
        var locateButtonText = locateClickedElement.find('span');
        var locateButtonIcon = locateClickedElement.find('i.icon-white');
        
        if(locateClickedElement.hasClass('btn-success')){
            locateClickedElement.removeClass('btn-success');
            locateClickedElement.addClass('btn-danger');
            locateButtonText.html('Stop');
            locateButtonIcon.removeClass('icon-play').addClass('icon-stop');
            locateAllButtons.addClass('disabled');
            locateClickedElement.removeClass('disabled');
			change_status(tasks, 'In-Progress');
        } else{
            locateClickedElement.removeClass('btn-danger');
            locateClickedElement.addClass('btn-success');
            locateButtonText.html('Start');
            locateButtonIcon.removeClass('icon-stop').addClass('icon-play');
            locateAllButtons.removeClass('disabled');
			change_status(tasks, 'On Hold')
        }

        // start time tracking
        locateClickedElement.prev('.time').stopwatch().stopwatch('toggle');
        
    });	
})(jQuery)
function change_status(tasks,status){
  $.ajax({	  
	  url : base_url+'/change_status',
	  type : "post",
	  data: {_token:csrf_token,tasks:tasks,status:status},
	  success: function(json){
		  $.each(tasks, function(i, v){
			  $('#task_status-'+v).text(status);
		  });
	  }
  });	
}
/*
* Timer Auto start
* Ashvin Patel 24/Mar/2015
*/
$(document).ready(function(e) {
	//setTimeout(timer_auto_start, 500)
	if(user_type != 1){
   		timer_auto_start(); 
	}
});

function timer_auto_start(){
	$('.task_row_wrap').each(function(index, elem) {
        var elem = $(this);
		var task_id = elem.attr('data-id');
		if($('#task_status-'+task_id).text() == 'In-Progress'){
			$('#task-row-'+task_id).find('.time').stopwatch().stopwatch('start');
			start_timer(task_id);
		}
    });	
}