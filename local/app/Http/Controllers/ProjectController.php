<?php namespace App\Http\Controllers;
use Input,DB;
use App\Models\Project;
use App\Models\Project_details;
class ProjectController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Project Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "project page" for the application and
	| is configured to only allow guests. 
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application task screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{		
		$data['projects'] = $this->get_all_projects();
		//print_r(json_encode($data['projects'])); die;
		//$data['projects'] = json_decode($data['projects']);
		///echo"<pre>"; print_r($data['projects']);die;
		
		return view('pages/project', $data);
	}
	
	
	public function edit_project(){
		
		if(Input::get('submit')){
			if(Input::get('id')){ //echo"hello";
				 $files = Input::file('project_file');
			
							$filesName = [];
							if(!empty($files)){
								foreach($files as $file){
									if($file){									
										if(upload_file($file, 'uploads'));
										$fileName[] =  $file->getClientOriginalName();
									}
								}
							}
				$update = DB::table('project')
							->where('id', Input::get('id'))
							->update([
									'name' => Input::get('name'),	
									'description' => Input::get('description'),	
									'link' => Input::get('link'),				
									'tag' => Input::get('tags'),	
									'manager' => Input::get('manager'),	
									'developer' => Input::get('developer'),	
									'project_file'	=> isset($fileName) ? is_array($fileName) ? implode(',', $fileName) : '' : '',
									'status' => Input::get('status'),
									'created_by_id' => 1
									]);
							
			if(Input::get('type')){
					$types = Input::get('type');
					$username = Input::get('username');
					$password = Input::get('password');
					$remark = Input::get('remark');
					$cr_link = Input::get('cr_link');
					$i = 0;
					DB::table('project_details')->where('project_id', Input::get('id'))->delete();
					foreach($types as $type) {
					$detail = DB::table('project_details')
											->insert(
													['project_id'=> Input::get('id'),
													'type' 		=> $types[$i],
													'username' 	=> $username[$i],
													'password' 	=> $password[$i],
													'cr_link' 	=> $cr_link[$i],
													'remark' 	=> $remark[$i]
													]
													);	
					$i++;
					}
			}
			echo '<script>parent.$.fancybox.close();parent.location.reload(true);</script>'; 
			}
		}else{
			if(Input::get('id')){
				$project['project'] = $this->getProject(Input::get('id'));
				$project['details'] = $this->getProjectDetails(Input::get('id'));
				return view('layouts/add_project', $project);
			}	
		}
		
	}
	
	
	public function delete_project(){
		
		        if(Input::get('id')){					
					$update = DB::table('project')
							->where('id', Input::get('id'))
							->update([
									'is_deleted' => 1,									
									]);
				}	
				echo $update;
		}
	
	
	
	
	
	
	
	/**
	 * Add tasks.
	 *
	 * @return Response
	 * Ashvin Patel 16/Mar/2015
	 */
	public function add_project()
	{	
	   
		
		if(Input::get('submit')){
			 $files = Input::file('project_file');
			
							$filesName = [];
							if(!empty($files)){
								foreach($files as $file){
									if($file){									
										if(upload_file($file, 'uploads'));
										$fileName[] =  $file->getClientOriginalName();
									}
								}
							}
		
	
			$project = new Project;		
			$result = $project->create([
						'name' => Input::get('name'),	
						'description' => Input::get('description'),				
						'link' => Input::get('link'),				
						'tag' => Input::get('tags'),	
						'manager' => Input::get('manager'),	
						'developer' => Input::get('developer'),	
						'project_file'	=> isset($fileName) ? is_array($fileName) ? implode(',', $fileName) : '' : '',						
						'status' => Input::get('status'),
						'created_by_id' => 1
					]);
		
			$types = Input::get('type');
			$username = Input::get('username');
			$password = Input::get('password');
			$remark = Input::get('remark');
			$cr_link = Input::get('cr_link');
			
			$i = 0;
			print_r($_POST);
			print_r($username);
			die;
			if(!empty($types)){
			foreach($types as $type) {
			$detail = DB::table('project_details')->insert(
											['project_id'=> $result->id,
											'type' 		=> $types[$i],
											'username' 	=> $username[$i],
											'password' 	=> $password[$i],
											'cr_link' 	=> $cr_link[$i],
											'remark' 	=> $remark[$i]
											]
											);	
			$i++;
			}
			}
			echo '<script>parent.$.fancybox.close();parent.location.reload(true);</script>'; 	
			
		}else{
			return view('layouts/add_project');
		}
	}
	
	/**
	 * Edit Project
	 *
	 * @return project info
	 * Pradeep Sharma 06/May/2015
	 */
	 public function getProject($id){
		 $projects = Project::where('is_deleted', 0)
		 			->where('id', $id)
					->first();	
					//echo"<pre>";print_r($projects); die;
		return $projects;
	 }
	 
	 /**
	 * Get project details 
	 *
	 * @return project details
	 * Pradeep Sharma 06/May/2015
	 */
	 public function getProjectDetails($id){
		  $details = DB::table('project_details')
		 			->where('project_id', $id)
					->get();	
					//echo"<pre>";print_r($projects); die;
		return $details;
	 }
	
	/**
	 * Edit tasks.
	 *
	 * @return Response
	 * Ashvin Patel 17/Mar/2015
	 */
	public function edit_task()
	{				
		$task_id = Input::get('task_id');
		$title = Input::get('task');
		if(!empty($task_id)){		
		   $task_data = array('title' => $title);
		   Task::where('id', $task_id)->update($task_data);		
		}
	}
	/*
	* Get All tasks from database
	* 
	* @return task bundle
	* Ashvin patel 17/Mar/2015
 	*/
	public function get_all_task(){
		$task = new Task;
		//$tasks = $task->get();	
		$tasks = $task->where('task.is_deleted', 0)
					  ->leftJoin('project as p', 'p.id', '=', 'task.project')
					  ->select('task.*', 'p.name as project_name')
					  ->orderBy('updated_at', 'DESC')->get();
		return $tasks;
	}
	/*
	* Change tasks status
	* 
	* @return null
	* Ashvin Patel 17/Mar/2015
	*/
	public function change_task_status(){
		//print_r(Input::all());
		$tasks = Input::get('tasks');
		if(!empty($tasks)){
		 foreach($tasks as $task){
			 $task_data=array('status' => Input::get('status'));
			 Task::where('id', $task)->update($task_data);
		 }
		}
	}
	/*
	* Add Task estimated time
	* 
	* @return time
	* Ashvin Patel 17/Mar/2015
	*/
	public function change_etime(){
		$task_id = Input::get('task_id');
		$time = Input::get('time');
		if(!empty($task_id)){		
		   $task_data = array('estimated_time' => $time);
		   Task::where('id', $task_id)->update($task_data);		
		}
	}
	
	/*
	* Assign user into Task
	* 
	* @return null
	* Ashvin Patel 17/Mar/2015
	*/
	public function assign_user(){		
		$tasks = Input::get('tasks');
		if(!empty($tasks)){
		 foreach($tasks as $task){
			 $task_data=array('assignee' => Input::get('assignee'));
			 Task::where('id', $task)->update($task_data);
		 }
		}
	}
	
	/*
	* Get All users from database
	* 
	* @return users bundle
	* Ashvin patel 17/Mar/2015
 	*/
	public function get_all_users(){
		$user = new User;		
		$users = $user->orderBy('name', 'ASC')->get();
		return $users;
	}
	
	/*
	* Search task by keyword
	* 
	* @return tasks bundle
	* Ashvin patel 18/Mar/2015
 	*/
	public function search_task(){	
		$keyword = Input::get('keyword');
		$data = Input::get('data');
		$where = array('task.is_deleted' => 0);	
		if($keyword)
		$where['task.title'] = 'LIKE "%'.$keyword.'%"';	
			
		if($data['status'])
		$where['task.status'] = $data['status'];
				
		if($data['assignee'])
		$where['task.assignee'] = $data['assignee'];
		
		if($data['project'])
		$where['task.project'] = $data['project'];
		
		$tasks = Task::where($where)
						->leftJoin('users as u', 'u.id', '=', 'task.assignee')
						->leftJoin('project as p', 'p.id', '=', 'task.project')
						->select('task.*', 'u.name as assignee_name', 'p.name as project_name')
						->orderBy('task.updated_at', 'DESC')
						->get();
		echo ($tasks) ? json_encode($tasks) : '';
	}
	
	/*
	* Get all project
	* 
	* @return project bundle
	* Ashvin patel 18/Mar/2015
 	*/	
	public function get_all_projects(){
		$projects = Project::where('is_deleted', 0)
					/*->select('project.id','project.name','project.description','project.status',
					'project_details.id','project_details.name','project_details.description','project_details.status')*/
					/*->leftJoin('project_details', 'project.id', '=', 'project_details.project_id')*/
					->groupBy('project.id')
					->get();	
		return $projects;
	}
	
	/*
	* Assign project to Task
	* 
	* @return null
	* Ashvin Patel 18/Mar/2015
	*/
	public function assign_project(){		
		$tasks = Input::get('tasks');
		if(!empty($tasks)){
		 foreach($tasks as $task){
			 $task_data=array('project' => Input::get('project'));
			 Task::where('id', $task)->update($task_data);
		 }
		}
	}
}
