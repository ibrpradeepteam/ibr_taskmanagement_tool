<?php namespace App\Http\Controllers;
use Input;
use Validator;
use Session;
use Redirect;
use DB;
use App\Models\Task;
use App\Models\User;
use App\Models\Project;
use App\Models\Comment;
use App\Models\Attachment;
class TaskController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Task Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "task page" for the application and
	| is configured to only allow guests. 
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{			
		if(Session::get('user_id')){
			$this->middleware('guest');
		}else{
			$this->middleware('auth');	
		}
	}

	/**
	 * Show the application task screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{		
		$data['user_m'] = new User;
		$data['file_m'] = new Attachment;
		$data['tasks'] = $this->get_all_task();
		$data['users'] = $this->get_all_users();
		$data['projects'] = $this->get_all_projects();
		return view('pages/tasks', $data);
	}
	
	/**
	 * Add tasks.
	 *
	 * @return Response
	 * Ashvin Patel 16/Mar/2015
	 */
	public function add_task()
	{		
		$files = Input::get('attach_files');
		$task_data = Input::get('data');
		$task = new Task;	
		if(Session::get('user_type') != 1){
			if($task_data['assignee']){
				$assignee = $task_data['assignee'];
			}else{
				$assignee = Session::get('user_id');
			}
		}else{
			if($task_data['assignee']){
				$assignee = $task_data['assignee'];
			}else{
				$assignee = NULL;
			}	
		}		
		$result['task'] = $task->create([
					'title' => Input::get('task'),								
					'status' => 'New',
					'assignee' => $assignee,
					'project' => $task_data['project'],
					'created_by_id' => Session::get('user_id')
				]);
		$task_id = $result['task']['id'];
		$result = $this->get_task_detail($task_id);
		if(is_array($files)){
			foreach($files as $file){
				 $file_data=array('parent_id' => $task_id);
			 	 Attachment::where('id', $file)->update($file_data);
			}
		}
		$result['files'] = $this->get_task_files($task_id);
		echo json_encode($result);
	}
	
	/*
	* Get single task detail
	* 
	* @return task detail
	* Ashvin Patel 20/mar/2015
	*/
	public function get_task_detail($id=''){
		if($id){
			$task = Task::where('task.id', $id)						
						->leftJoin('users as u', 'u.id', '=', 'task.assignee')
						->leftJoin('project as p', 'p.id', '=', 'task.project')
						->leftJoin('users as us', 'us.id', '=', 'task.created_by_id')
						->select('task.*', 'u.name as assignee_name', 'p.name as project_name', 'us.name as created_by')						
						->first();
			return $task;
		}
	}
	/**
	 * Edit tasks.
	 *
	 * @return Response
	 * Ashvin Patel 17/Mar/2015
	 */
	public function edit_task()
	{				
		$task_id = Input::get('task_id');
		$title = Input::get('task');
		if(!empty($task_id)){		
		   $task_data = array('title' => $title);
		   Task::where('id', $task_id)->update($task_data);		
		}
	}
	
	/*
	* Delete Budle of tasks or single task
	* 
	* @return null
	* Ashvin Patel 23/Mar/2015
	*/
	public function delete_tasks()
	{
		$tasks = Input::get('tasks');
		if(!empty($tasks)){
			Task::whereIn('id', $tasks)->update(array('is_deleted' => 1));
		}
	}
	
	/*
	* Get All tasks from database
	* 
	* @return task bundle
	* Ashvin patel 17/Mar/2015
 	*/
	public function get_all_task(){
		$task = new Task;
		//$tasks = $task->get();		
		if(Session::get('user_type') != 1){
			$where = array('task.assignee' => Session::get('user_id'));	
		}
		$where['task.is_deleted'] = 0;
		$tasks = $task->where($where)					  
					  ->whereRaw('task.status NOT IN ("Complete", "Canceled")')
					  ->leftJoin('project as p', 'p.id', '=', 'task.project')
					  ->leftJoin('users as u', 'u.id', '=', 'task.created_by_id')
					  ->select('task.*', 'p.name as project_name','p.id as project_id', 'u.name as created_by')
					  ->orderBy('priority', 'ASC')
					  ->orderBy('updated_at', 'DESC')->get();
		if($tasks){
			foreach($tasks as $key => $task){				
				 $tasks[$key]['files'] = $this->get_task_files($task['id']);
				 $tasks[$key]['worked_time'] = $this->get_task_worked_time($task['id'], $task['assignee']);
			}
		}
		return $tasks;
	}
	/*
	* Change tasks status
	* 
	* @return null
	* Ashvin Patel 17/Mar/2015
	*/
	public function change_task_status(){
		//print_r(Input::all());
		$tasks = Input::get('tasks');
		if(!empty($tasks)){
		 foreach($tasks as $task){
			 $task_data=array('status' => Input::get('status'));
			 Task::where('id', $task)->update($task_data);
		 }
		}
	}
	/*
	* Add Task estimated time
	* 
	* @return time
	* Ashvin Patel 17/Mar/2015
	*/
	public function change_etime(){
		$task_id = Input::get('task_id');
		$time = Input::get('time');
		if(!empty($task_id)){		
		   $task_data = array('estimated_time' => $time);
		   Task::where('id', $task_id)->update($task_data);		
		}
	}
	
	/*
	* Assign user into Task
	* 
	* @return null
	* Ashvin Patel 17/Mar/2015
	*/
	public function assign_user(){		
		$tasks = Input::get('tasks');
		if(!empty($tasks)){
		 foreach($tasks as $task){
			 $task_data=array('assignee' => Input::get('assignee'));
			 Task::where('id', $task)->update($task_data);
		 }
		}
	}
	
	/*
	* Get All users from database
	* 
	* @return users bundle
	* Ashvin patel 17/Mar/2015
 	*/
	public function get_all_users(){
		$user = new User;		
		$users = $user->orderBy('name', 'ASC')->get();
		return $users;
	}
	/*
	* Search task by keyword
	* 
	* @return tasks bundle
	* Ashvin patel 18/Mar/2015
 	*/
	public function keyword_search(){	
		$keyword = Input::get('keyword');		
		/*if($keyword)
		$where['task.title'] = 'LIKE "%'.$keyword.'%"';	*/
		$where = array('task.is_deleted' => 0);	
		$tasks = Task::where('task.is_deleted', '=', 0);
		if($keyword){			
			$keyword = explode(',', str_replace(' ', '', $keyword));			
			foreach($keyword as $key => $value){
				if(!$value){
					unset($keyword[$key]);	
				}
			}
			$keyword = array_values($keyword);			
			$keyword = implode('|', $keyword);			
			$tasks = $tasks->whereRaw('(REPLACE(task.title, " ", "") REGEXP "'.$keyword.'" or REPLACE(task.status, " ", "")  REGEXP "'.$keyword.'" or REPLACE(u.name, " ", "") REGEXP "'.$keyword.'" or REPLACE(p.name, " ", "") REGEXP "'.$keyword.'")');
		}
			$tasks = $tasks->leftJoin('users as u', 'u.id', '=', 'task.assignee')
							->leftJoin('project as p', 'p.id', '=', 'task.project')
							->leftJoin('users as us', 'us.id', '=', 'task.created_by_id')
							->select('task.*', 'u.name as assignee_name', 'us.name as created_by', 'p.name as project_name')
							->orderBy('task.priority', 'ASC')
							->orderBy('task.updated_at', 'DESC')
							->get();
			//$queries = DB::getQueryLog();
			//$last_query = end($queries);
			//print_r($last_query['query']);
			if($tasks){
				foreach($tasks as $key => $task){				
					 $tasks[$key]['files'] = $this->get_task_files($task['id']);
					 $tasks[$key]['worked_time'] = $this->get_task_worked_time($task['id'], $task['assignee']);
				}
			}
			echo ($tasks) ? json_encode($tasks) : '';
	}
	
	/*
	* Adavnce Search
	* 
	* @return tasks bundle
	* Ashvin patel 18/Mar/2015
 	*/
	public function search_task(){	
		$keyword = Input::get('keyword');
		$data = Input::get('data');
		$where = array('task.is_deleted' => 0);	
		/*if($keyword)
		$where['task.title'] = 'LIKE "%'.$keyword.'%"';	*/
		/*if($keyword){
			$tasks = Task::where($where)->where('task.title', 'LIKE', '%'.$keyword.'%');
		}else{*/
		$tasks = Task::where($where);	
		//}
		$today_complete_tasks = array();
		if($data['date']){
			$today = strtotime(date('Y-m-d'));
			$s_date = strtotime($data['date']);
			$dt_where = '(task.updated_at LIKE "%'.date('Y-m-d', strtotime($data['date'])).'%"';
			if($today == $s_date){				
				if(!$data['status']){
					$dt_where = ' task.status IN("new", "In-Progress", "Wait for response", "On Hold")';
					$tasks = $tasks->whereRaw($dt_where);
					$today_complete_tasks = $this->get_today_complete_tasks($data);
				}else{
					$tasks = $tasks->where('task.updated_at', 'LIKE',  "%".date('Y-m-d', strtotime($data['date']))."%");	
				}
			}else{
				$tasks = $tasks->where('task.updated_at', 'LIKE',  "%".date('Y-m-d', strtotime($data['date']))."%");
				/*$status_wher = implode("', '", $data['status']);
				$status_wher =  "'".$status_wher."'";				
				$dt_where .= ' or task.status IN('.$status_wher.'))';
				$tasks = $tasks->whereRaw($dt_where);*/
			}
		}
		if(!empty($data['status'])){
			$tasks = $tasks->whereIn('task.status', $data['status']);
		}
		if(!empty($data['assignee'])){
			$tasks = $tasks->whereIn('task.assignee', $data['assignee']);
		}
		if(!empty($data['project'])){
			$tasks = $tasks->whereIn('task.project', $data['project']);
		}
		
		$tasks = $tasks->leftJoin('users as u', 'u.id', '=', 'task.assignee')
						->leftJoin('project as p', 'p.id', '=', 'task.project')
						->leftJoin('users as us', 'us.id', '=', 'task.created_by_id')
						->select('task.*', 'u.name as assignee_name', 'us.name as created_by', 'p.name as project_name')
						->orderBy('task.priority', 'ASC')
						->orderBy('task.updated_at', 'DESC')
						->get();
		$key = 0;
		if($tasks){
			foreach($tasks as $key => $task){	
				 $key = $key;			
				 $tasks[$key]['files'] = $this->get_task_files($task['id']);
				 $tasks[$key]['worked_time'] = $this->get_task_worked_time($task['id'], $task['assignee']);
			}
		}
		
		if(!empty($today_complete_tasks)){
			foreach($today_complete_tasks as $today_complete_task){
				$tasks[$key+1] = $today_complete_task;
			}
			//$tasks = $tasks + $today_complete_tasks;
			//$tasks = array_merge($tasks, $today_complete_tasks/*, $arrayN, $arrayN*/);
		}
		echo ($tasks) ? json_encode($tasks) : '';
	}
	
	public function get_today_complete_tasks($data){
		$where = array('task.is_deleted' => 0, 'task.status' => 'Complete');	
		$dt_where = ' task.updated_at LIKE "%'.date('Y-m-d').'%"';	
		$tasks = Task::where($where)->whereRaw($dt_where);
		
		if(!empty($data['assignee'])){
			$tasks = $tasks->whereIn('task.assignee', $data['assignee']);
		}
		if(!empty($data['project'])){
			$tasks = $tasks->whereIn('task.project', $data['project']);
		}
		$tasks = $tasks->leftJoin('users as u', 'u.id', '=', 'task.assignee')
				 ->leftJoin('project as p', 'p.id', '=', 'task.project')
				 ->leftJoin('users as us', 'us.id', '=', 'task.created_by_id')
				->select('task.*', 'u.name as assignee_name', 'us.name as created_by', 'p.name as project_name')
				->orderBy('task.priority', 'ASC')
				->orderBy('task.updated_at', 'DESC')
				->get();
	  	if($tasks){
		  foreach($tasks as $key => $task){				
			  $tasks[$key]['files'] = $this->get_task_files($task['id']);
			  $tasks[$key]['worked_time'] = $this->get_task_worked_time($task['id'], $task['assignee']);
		  }
		}
		return $tasks;	
	}
	
	/*
	* Get all project
	* 
	* @return project bundle
	* Ashvin patel 18/Mar/2015
 	*/	
	public function get_all_projects(){
		$projects = Project::where('is_deleted', 0)->get();	
		return $projects;
	}
	
	/*
	* Assign project to Task
	* 
	* @return null
	* Ashvin Patel 18/Mar/2015
	*/
	public function assign_project(){		
		$tasks = Input::get('tasks');
		if(!empty($tasks)){
		 foreach($tasks as $task){
			 $task_data=array('project' => Input::get('project'));
			 Task::where('id', $task)->update($task_data);
		 }
		}
	}
	
	/*
	* Sort task using sortale
	* 
	* Return null
	* Ashvin patel 19/Mar/2015
	*/
	public function task_sortable(){
		$tasks = Input::get('tasks');
		if(!empty($tasks)){
		 foreach($tasks as $key => $task){
			 $task_data=array('priority' => $key);
			 Task::where('id', $task)->update($task_data);
		 }
		}
	}
	
	/** Start Comment Section on 19/Mar/2015 Ashvin Patel**/
	
	/*
	* Add Comment in task or by user
	*
	* @return comment detail
	* Ashvin Patel 18/Mar/2015
	*/
	public function add_comment(){
		$chat = new Comment;
		$comment = $chat->create([
						'task_id' => Input::get('task_id'),
						'name' => Session::get('user_name'),
						'email' => Session::get('user_email'),
						'msg' => Input::get('msg'),
						'created_by_id' => Session::get('user_id'),
					]);	
		echo json_encode($comment);
	}
	
	/*
	* Get Comments by task
	* 
	* @return task comments bundle
	* Ashvin Patel 18/Mar/2015
	*/
	public function get_task_comment(){
		$task_id = Input::get('task_id');
		$comments = Comment::where('task_id', $task_id)->get();
		echo json_encode($comments);	
	}
	
	/*
	* Get task Latest comment
	*
	* @return latest comment
	* Ashvin Patel 20/Mar/2015
	*/
	public function get_task_latest_comment(){
		$task_id = Input::get('task_id');
		$last_comment_id = Input::get('last_comment_id');		
		$comments = Comment::where('task_id', $task_id)->where('id', '>', $last_comment_id)->get();
		echo json_encode($comments);
	}
	
	/**End Comment Section 20/Mar/2015**/
	
	/*
	* Upload file
 	*
	* @return file details
	* Ashvin Patel 19/Mar/2015
	*/
	public function upload_file() {						
	  // getting all of the post data
	  $file = array('image' => Input::file('file'));
	  // setting up rules	 
	  $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
	  // doing the validation, passing post data, rules and the messages
	  $validator = Validator::make($file, $rules);
	  if ($validator->fails()) {
		// send back to the page with the input data and errors
		//return Redirect::to('upload')->withInput()->withErrors($validator);
		 print_r($validator);
	  }
	  else {
		// checking file is valid.
		if (Input::file('file')->isValid()) {
		  $destinationPath = 'uploads'; // upload path
		  $extension = Input::file('file')->getClientOriginalExtension(); // getting image extension
		  $fileName =  Input::file('file')->getClientOriginalName(); // renameing image
		  Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
		  $task_id = Input::get('task_id');
		  $this->save_file($fileName, $task_id);
		  // sending back with message
		 // Session::flash('success', 'Upload successfully'); 
		  //return Redirect::to('upload');
		}
		else {
		  // sending back with error message.
		 // Session::flash('error', 'uploaded file is not valid');
		  //return Redirect::to('upload');
		}
	  }
	}
	
	/*
	* Save Uploaded file in database 
	*
	* @return file detail
	* Ashvin patel 19/Mar/2015
	*/
	public function save_file($filename='', $task_id='')
	{
		if($filename)
		{			
			$file = Attachment::create([							
							'name' => $filename,							
							'created_by_id' => Session::get('user_id'),
							'parent_id' => ($task_id) ? $task_id : NULL
						]);	
			echo json_encode($file);	
		}
	}
	/*
	* Get all files related with task
	* 
	* @return task files
	* Ashvin Patel 20/mar/2015
	*/
	public function get_task_files($task_id=''){
		if($task_id){
			$files = Attachment::where('parent_id', $task_id)->where('is_deleted', 0)->get();	
			return $files;
		}
	}
	
	/*
	* Delete attachment of task
	* 
	* @return null
	* Ashvin patel 21/Mar/2015
	*/
	public function delete_attach(){
		$file_id = Input::get('file_id');
		if($file_id){
			Attachment::where('id', $file_id)->update(array('is_deleted' => 1));			
		}
	}
	
	/*
	* Track Task time minutly
	* 
	* @return null
	* Ashvin Patel 23/Mar/2015
	*/
	public function task_time_track(){
		$task_id = Input::get('task_id');
		$user_id = Input::get('user_id');
		$results = DB::table('user_timer_log')
					->where('task_id', '=', $task_id)
					->where('user_id', '=', $user_id)					
					->first();
		if(!empty($results)){
			$timer_log = DB::table('user_timer_log')->where('task_id', $task_id)->where('user_id', $user_id)->increment('worked_time');
		}else{
			$log_data = array('task_id' => $task_id, 'user_id' => $user_id, 'worked_time' => 1, 'created_at' => date('Y-m-d H:i:s'));
			$timer_log = DB::table('user_timer_log')->insert($log_data);	
		}
		echo json_encode($timer_log);
	}
	
	/*
	* Get Task Worked time by Assignee
	* 
	* @return worked time in minutes
	* Ashvin patel 24/Mar/2015
	*/
	public function get_task_worked_time($task_id='', $user_id='')
	{
		$worked_time = DB::table('user_timer_log')->where('task_id', $task_id)->where('user_id', $user_id)->first();
		return isset($worked_time->worked_time) ? $worked_time->worked_time : 0;
	}
}
