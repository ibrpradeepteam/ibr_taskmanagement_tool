<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Attachment extends Model {

	protected $fillable = array('name', 'parent_id', 'created_by_id');
    protected $table = 'attachment';
	public function get_task_files($task_id=''){
		if($task_id){
			$files = $this->where('parent_id', $task_id)->get();	
			return $files;
		}
	}
}