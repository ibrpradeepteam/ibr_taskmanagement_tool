<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Project_details extends Model {

	protected $fillable = array('username', 'type', 'project_id', 'password','remark','cr_link');
    protected $table = 'project_details';

}