<?php namespace App\Http\Controllers;

use Input;
use Validator;
use Session;
use Redirect;
use App\Models\User;
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('pages/login');
	}
	public function login()
	{
		if(Input::get('email')&&Input::get('password')){
			$_token = Session::get('_token');
			Session::flush();
			$userObj = new User;
			$userdetail = $userObj->get_user_detail(Input::get('email'), Input::get('password'));
			//print_r($userdetail);			
			if($userdetail){
				//print_r($userdetail['id']);			
				Session::put('user_id', $userdetail['id']);
				Session::put('user_name', $userdetail['name']);
				Session::put('user_email', $userdetail['email']);
				Session::put('user_type', $userdetail['type']);
				Session::put('user_type', $userdetail['type']);
				Session::put('_token', $_token);
				return Redirect::to(url('/'));	
			}else{
				return Redirect::to(url('/auth/login'));	
				 //$this->middleware('auth');
			}
		}else{			
			if(sizeof(Session::all()) > 1){
				return Redirect::to(url('/'));	
			}else{				
				return Redirect::to(url('/auth/login'));	
			}
		}	
	}
	public function logout(){
		Session::flush();	
		return Redirect::to(url('/auth/login'));
	}

}
