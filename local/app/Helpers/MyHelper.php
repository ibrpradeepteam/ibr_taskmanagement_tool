<?php
/*
* Upload file
*
* @return file details
* Ashvin Patel 19/Mar/2015
*/
function upload_file($filename='', $destinationPath='uploads') {						
  // getting all of the post data
  $file = array('image' => $filename);
  // setting up rules	 
  $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
  // doing the validation, passing post data, rules and the messages
  $validator = Validator::make($file, $rules);
  if ($validator->fails()) {
	// send back to the page with the input data and errors
	//return Redirect::to('upload')->withInput()->withErrors($validator);
	 return false;
  }
  else {
	// checking file is valid.
	if ($filename->isValid()) {	  
	  $extension = $filename->getClientOriginalExtension(); // getting image extension
	  $fileName =  $filename->getClientOriginalName(); // renameing image
	  $filename->move($destinationPath, $fileName); // uploading file to given path	  
	  return true;
	}
	else {
	  // sending back with error message.
	 	return false;	 
	}
  }
}
function current_user_avatar(){	
  if(Session::get('user_pic')){
	  if(file_exists('uploads/profile_pic/'.Session::get('user_pic'))){
		  echo  url().'/uploads/profile_pic/'.Session::get('user_pic');	
	  }else{
		  echo url().'/uploads/profile_pic/avatar.jpg';		
	  }
  }else{
	  echo url().'/uploads/profile_pic/avatar.jpg';			
  }
}

function user_avatar($pic_name){	
  if($pic_name){
	  if(file_exists('uploads/profile_pic/'.$pic_name)){
		  echo  url().'/uploads/profile_pic/'.$pic_name;	
	  }else{
		  echo url().'/uploads/profile_pic/avatar.jpg';		
	  }
  }else{
	  echo url().'/uploads/profile_pic/avatar.jpg';			
  }
}
function UserAvatarByID($id=''){
  $pic = '';
  if($id){	
  	$pic = DB::table('user_profile')->where('user_id', $id)->select('pic')->first();
	$pic = isset($pic->pic) ? $pic->pic : '';
  }
  if($pic){
	  if(file_exists('uploads/profile_pic/'.$pic)){
		  echo  url().'/uploads/profile_pic/'.$pic;	
	  }else{
		  echo url().'/uploads/profile_pic/avatar.jpg';		
	  }
  }else{
	  echo url().'/uploads/profile_pic/avatar.jpg';			
  }
}
function sendMail($data){
		require_once('mail_obj.php');
		$smtp  = new PHPMailer();
		$smtp->IsSMTP(); 
		$smtp->SMTPAuth   = true;  
		$smtp->Host       = 'smtp.gmail.com';
		$smtp->Port       = '465';
		$smtp->SMTPSecure = 'ssl';
		$smtp->Username   = 'ibr.ashvin@gmail.com'; 
		$smtp->Password   = '9907085461'; 
		$from             = 'support@tutor.com';  
		$from_name        = 'Tutor';
		
		
		$smtp->SetFrom($from, $from_name);
		$smtp->AddReplyTo($from, $from_name);
    	$smtp->Subject = $data['sub'];
    	$smtp->MsgHTML($data['msg']);	
		//$smtp->AddAddress($data['email'], "");  
		$smtp->AddAddress("sulbha@ibrinfotech.com"); 
		 
		if($_SERVER['HTTP_HOST'] != 'localhost'){
			
			$msg = $data['msg'];
			
			$email_to = $data['email'];
			$email_subject = $data['sub'];
			$email_message = $msg;
			$headers = "From: Beacze\r\n".
			"Reply-To: support@tutor.sg\r\n'" .
			"X-Mailer: PHP/" . phpversion();
			 mail($email_to, $email_subject, $email_message, $headers);  
			"mail sent!";
		}else{
			
			//print_r($smtp->Send());
			if(!$smtp->Send()){	
				$res = "<p style='color:red;text-align: center;margin-top: 28px;text-shadow: 1px 0px 11px red;'>Mailer Error: " . $smtp->ErrorInfo."</p>";	
			}else{	
				$res =  "<h3 style='color:green;text-align: center;margin-top: 28px;text-shadow: 1px 0px 11px green;'>Message sent!</h3>";
			}
			//echo $res;
		}
}

function get_weekdays($start='Sunday')
{
	if($start == 'Sunday'){
		return [7 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday'];	
	}else{
		return [1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday'];	
	}
}

/*
* Get any User name by id
*/
function get_user_name($user_id=''){
	if($user_id){
		$user = DB::table('site_user')->where('id', $user_id)->select('prefix', 'f_name', 'l_name')->first();	
		return $user->f_name.' '.$user->l_name;
	}
}

/*
* Get any User name by id
*/
function get_user_email($user_id=''){
	if($user_id){
		$user = DB::table('site_user')->where('id', $user_id)->select('email')->first();	
		return $user->email;
	}
}
function get_day_name($day){
	switch($day){
		case 7:
		return 'Sunday';
		break;
		case 1:
		return 'Monday';
		break;
		case 2:
		return 'Tuesday';
		break;
		case 3:
		return 'Wednesday';
		break;
		case 4:
		return 'Thursday';
		break;
		case 5:
		return 'Friday';
		break;
		case 6:
		return 'Saturday';
		break;	
	}
}
function getServiceScheduleSlots($duration,$break, $stTime,$enTime)
{
        $start = new DateTime($stTime);
        $end = new DateTime($enTime);
        $interval = new DateInterval("PT" . $duration. "M");
        $breakInterval = new DateInterval("PT" . $break. "M");
		$periods = [];
        for ($intStart = $start; 
             $intStart < $end; 
             $intStart->add($interval)->add($breakInterval)) {

               $endPeriod = clone $intStart;
               $endPeriod->add($interval);
               if ($endPeriod > $end) {
                 $endPeriod=$end;
               }
               $periods[] = $intStart->format('h:iA') . 
                            ' - ' . 
                            $endPeriod->format('h:iA');
        }

        return $periods;
  }
function availDaysToSlots($duration, $break, $avail_days){
	$slots = [];
	if(!empty($avail_days)){
		foreach($avail_days as $key => $days_0){
			if(is_array($days_0)){
				foreach($days_0 as $day){	
					$slots[$day->day][] = getServiceScheduleSlots($duration,$break, $day->start,$day->end);		
				}
			}
		}
	}
	$_slots = [];
	if(is_array($slots)){
		foreach($slots as $day => $slot){
			if(is_array($slot)){
				foreach($slot as $slot_){
					if(is_array($slot_)){
						foreach($slot_ as $_slot_){							
							$_slots[$day][] = $_slot_;	
						}
					}
				}
			}
		}
	}
	return $_slots;
}
function getDailyAvailSlots($duration,$break,$avail_days_slots,$reserveSlots, $until){
	$al_slots = [];
	if(is_array($avail_days_slots)){
		foreach($avail_days_slots as $day => $avail_days_slot){
			if(is_array($avail_days_slot)){
				$last_key = count($avail_days_slot) - 1;
				foreach($avail_days_slot as $key => $avail_days_slot_){
					//print_r($avail_days_slot_);
					$_avail_days_slot_ = explode('-', str_replace(' ', '', $avail_days_slot_));
					//print_r($_avail_days_slot_);
					if(checkSlotIsReserved($day, $_avail_days_slot_, $reserveSlots, $key, $avail_days_slot, $break, $until)){
						$_avail_days_slot_[0] = strtotime('+1hour', strtotime($_avail_days_slot_[0]));
						$_avail_days_slot_[0] = date('h:iA', $_avail_days_slot_[0]);	
						//print_r($_avail_days_slot_);					
						if(checkSlotIsReserved($day, $_avail_days_slot_, $reserveSlots, $key, $avail_days_slot, $break, $until)){
							$al_slots[$day][] = ['availibility' => 'not_available', 'slot' => $avail_days_slot_];		
						}else{
							if(strtotime($_avail_days_slot_[0]) != strtotime($_avail_days_slot_[1])){
								$_avail_days_slot_[1] = strtotime('+1hour', strtotime($_avail_days_slot_[1]));
								$_avail_days_slot_[1] = date('h:iA', $_avail_days_slot_[1]);	
								
								
								$s_end_time = explode('-', str_replace(' ', '', $avail_days_slot[$last_key]));
								$s_end_time = $s_end_time[1];
								$_av_slot_end = $_avail_days_slot_[1];
								$_avail_days_slot_ = implode(' - ', $_avail_days_slot_);
								$al_slots[$day][] = ['availibility' => '', 'slot' => $_avail_days_slot_];	
								if(strtotime($s_end_time) == strtotime($_av_slot_end)){
									break;
								}
							}
						}					
					}else{
						$al_slots[$day][] = ['availibility' => '', 'slot' => $avail_days_slot_];	
					}
					//print_r($al_slots);
				}
			}
		}
	}
	return ($al_slots);
}
function checkSlotIsReserved($slots, $start_date, $until){	
	//print_r($slots);
	$r_slots = [];
	if(is_array($slots)){
		foreach($slots as $day => $slot){
			if(is_array($slot)){
				foreach($slot as $key => $slot_){
					//print_r($slot_); echo '<br>';
					$slot_st = checkSlotReserved($day, $slot_, $start_date, $until);
					//echo '<br>';
					//($key == 2) ? die : '';
					if($slot_st == 1){
						$r_slots[$day][] = ['availibility' => 'not_available', 'slot' => $slot_];	
					}elseif($slot_st == 2){
						$r_slots[$day][] = ['availibility' => 'partial', 'slot' => $slot_];	
					}else{
						$r_slots[$day][] = ['availibility' => '', 'slot' => $slot_];	
					}					
				}
			}
		}
	}
	return $r_slots;
}
function checkSlotReserved($day='', $slot='', $start_date, $until){
	if($day !='' && $slot != ''){
		$day = get_day_name($day);
		$slot = explode('-', str_replace(' ', '', $slot));
		$start_date = date('Y/m/d', strtotime($start_date));
		$until = date('Y/m/d', strtotime($until));		
		$start = date('H:i:s', strtotime($slot[0]));
		$end = date('H:i:s', strtotime($slot[1]));
		
		
		//Step 3
		$where = "(
				    (DATE_FORMAT(event_start, '%Y/%m/%d') > '$start_date' and DATE_FORMAT(until, '%Y/%m/%d') < '$until') 
					
				 or (DATE_FORMAT(event_start, '%Y/%m/%d') < '$start_date' and DATE_FORMAT(until, '%Y/%m/%d') > '$start_date' and DATE_FORMAT(until, '%Y/%m/%d') < '$until')
				 
				 or (DATE_FORMAT(event_start, '%Y/%m/%d') <= '$start_date' and DATE_FORMAT(until, '%Y/%m/%d') > '$start_date' and DATE_FORMAT(until, '%Y/%m/%d') < '$until')
				 
				 or (DATE_FORMAT(event_start, '%Y/%m/%d') > '$start_date' and DATE_FORMAT(event_start, '%Y/%m/%d') < '$until' and DATE_FORMAT(until, '%Y/%m/%d') > '$until')
				 
				 or (DATE_FORMAT(event_start, '%Y/%m/%d') > '$start_date' and DATE_FORMAT(event_start, '%Y/%m/%d') < '$until' and DATE_FORMAT(until, '%Y/%m/%d') >= '$until')
				
				 )";
				 
		$where .= "and (
				 (DATE_FORMAT(start, '%H:%i:%s') >= '$start' and DATE_FORMAT(end, '%H:%i:%s') <= '$end') 
				 
				 or (DATE_FORMAT(start, '%H:%i:%s') <= '$start' and DATE_FORMAT(end, '%H:%i:%s') >= '$start' and DATE_FORMAT(end, '%H:%i:%s') <= '$end')
				 
				 or (DATE_FORMAT(start, '%H:%i:%s') >= '$start' and DATE_FORMAT(start, '%H:%i:%s') <= '$end' and DATE_FORMAT(end, '%H:%i:%s') >= '$end')
				 
				 or (DATE_FORMAT(start, '%H:%i:%s') <= '$start' and DATE_FORMAT(end, '%H:%i:%s') >= '$end')
				 )";
		$where .= " and event_day = '$day' and is_deleted = 0";
		
		$sql = "select id from booking where $where";
		
		$result = DB::select( DB::raw($sql));	
		
		if($result){
			$check_full = true;
			$partial = true;
		}else{
			$check_full = true;	
			$partial = false;
		}
		//end step 3
		if($check_full){
		//Step 1		
		$where = "(
					(DATE_FORMAT(event_start, '%Y/%m/%d') <= '$start_date' and DATE_FORMAT(until, '%Y/%m/%d') >= '$until') 
					
				 or	(DATE_FORMAT(event_start, '%Y/%m/%d') <= '$start_date' and DATE_FORMAT(until, '%Y/%m/%d') >= '$start_date' and DATE_FORMAT(until, '%Y/%m/%d') = '$until')
				 or (DATE_FORMAT(event_start, '%Y/%m/%d') = '$start_date' and DATE_FORMAT(event_start, '%Y/%m/%d') <= '$until' and DATE_FORMAT(until, '%Y/%m/%d') >= '$until')				
				 )";
				 
		$where .= "and (
				 (DATE_FORMAT(start, '%H:%i:%s') >= '$start' and DATE_FORMAT(end, '%H:%i:%s') <= '$end') 
				 
				 or (DATE_FORMAT(start, '%H:%i:%s') <= '$start' and DATE_FORMAT(end, '%H:%i:%s') >= '$start' and DATE_FORMAT(end, '%H:%i:%s') <= '$end')
				 
				 or (DATE_FORMAT(start, '%H:%i:%s') >= '$start' and DATE_FORMAT(start, '%H:%i:%s') <= '$end' and DATE_FORMAT(end, '%H:%i:%s') >= '$end')
				 
				 or (DATE_FORMAT(start, '%H:%i:%s') <= '$start' and DATE_FORMAT(end, '%H:%i:%s') >= '$end')
				 )";
		$where .= " and event_day = '$day' and is_deleted = 0";
		
		 $sql = "select id from booking where $where";
		
		$result = DB::select( DB::raw($sql)); 	
		if($result){
			return 1;
		}else{
			if($partial){
				//return 2;	
			}
		}
		//end step 1
		
		//Step 2
		$where = "((DATE_FORMAT(event_start, '%Y/%m/%d') <= '$start_date' and DATE_FORMAT(until, '%Y/%m/%d') >= '$until'))";
				 
		$where .= "and (
				 (DATE_FORMAT(start, '%H:%i:%s') >= '$start' and DATE_FORMAT(end, '%H:%i:%s') <= '$end') 
				 
				 or (DATE_FORMAT(start, '%H:%i:%s') <= '$start' and DATE_FORMAT(end, '%H:%i:%s') >= '$start' and DATE_FORMAT(end, '%H:%i:%s') <= '$end')
				 
				 or (DATE_FORMAT(start, '%H:%i:%s') >= '$start' and DATE_FORMAT(start, '%H:%i:%s') <= '$end' and DATE_FORMAT(end, '%H:%i:%s') >= '$end')
				 
				 or (DATE_FORMAT(start, '%H:%i:%s') <= '$start' and DATE_FORMAT(end, '%H:%i:%s') >= '$end')
				 )";
		$where .= " and event_day = '$day' and is_deleted = 0";
		
		$sql = "select id from booking where $where";
		
		$result = DB::select( DB::raw($sql)); 	
		if($result){
			return 1;
		}else{
			if($partial){
				return 2;	
			}
		}
		//end step 2
		}else{
			return 0;	
		}
		return 0;
	}
}
function checkInConflict($date='', $c_slots=''){
	if($date != '' && $c_slots != ''){
		if(is_array($c_slots)){
			foreach($c_slots as $c_slot){
				$slot = explode('-', $c_slot);
				$slot_date = explode(':', $slot[3]);
				$slot_date = $slot_date[1];
				if($slot_date == strtotime($date)){
					return true;	
				}
			}
		}
	}
}
function getUserLevels($user_id){
	return DB::table('level_sub_rel')->where(['user_id' => $user_id, 'is_deleted' => 0])->groupBy('level_id')->get();
}
function getLevelName($level_id){
	if($level_id == 1){
		return 'Lower Primary';	
	}elseif($level_id == 2){
		return 'Upper Primary';	
	}elseif($level_id == 3){
		return 'Lower Secondary';	
	}elseif($level_id == 4){
		return 'Lower Secondary';	
	}
}
function form_safe_json($json) {
    $json = empty($json) ? '[]' : $json ;
    $search = array('\\',"\n","\r","\f","\t","\b","'") ;
    $replace = array('\\\\',"\\n", "\\r","\\f","\\t","\\b", "&#039");
    $json = str_replace($search,$replace,$json);
    return strip_tags($json);
}
function getUserHourlyRate($user_id=''){
	if($user_id){
		$user_levels = getUserLevels($user_id);
		$rates = [];
		if(!empty($user_levels)){
			foreach($user_levels as $rate){
				$rates[] = $rate->rate;
			}
		}
		sort($rates);		
		$hourly_rate  = isset($rates[0]) ? $rates[0] : '';
		if($hourly_rate != end($rates)){ 
			$hourly_rate .= ' - '.end($rates);		
		}
		return $hourly_rate;
	}
}
function days($from, $to) {
	$from_date=strtotime($from);
	$to_date=strtotime($to);
	$current=$from_date;
	while($current<=$to_date) {
		$days[]=date('l', $current);
		$current=$current+86400;
	}
	return $days;
}
function dates($from, $to) {
	$from_date=strtotime($from);
	$to_date=strtotime($to);
	$current=$from_date;
	$dates = [];
	while($current<=$to_date) {
		$dates[]=date('Y-m-d', $current);
		$current=$current+86400;
	}
	return $dates;
}
function countDays($from, $to, $day) {
	$from_date=strtotime($from);
	$to_date=strtotime($to);
	$current=$from_date;
	$i = 0;
	while($current<=$to_date) {
		if(strtolower($day) == strtolower(date('l', $current))){
			$i++;		
		}
		$current=$current+86400;
	}
	return $i;
}
function sendWelcomeEmail($data){
	$data['sub'] = 'Welcome';
	$data['msg'] = '<p>Hello, '.Input::get('f_name').' '.Input::get('l_name').'</p>
						<p>Your account created by '.Session::get('user_name').' on Tutor</p>
						<p>Account detail as follows:</p>
						<p>Username: '.Input::get('email').'</p>
						<p>Password:  '.Input::get('password').'</p>';
	sendMail($data);
}
function getLevels(){
	return DB::table('levels')->orderBy('id', 'ASC')->get();	
}
function getLevelsName($level_id){
	if($level_id){
		$result = DB::table('levels')->where('id', $level_id)->first();	
		return $result->level;
	}
}
function getSubsName($level_id){
	if($level_id){
		$result = DB::table('levels')->where('id', $level_id)->first();	
		return $result->level;
	}
}
function getSublevels($level_id){
	if($level_id){
		return DB::table('sublevel')->where('level_id', $level_id)->orderBy('id', 'ASC')->get();
	}
}
function getSubLevelsName($sublevel_id){
	if($sublevel_id){
		$result = DB::table('sublevel')->where('id', $sublevel_id)->first();	
		return $result->name;
	}
}
function getSubjects($sublevel_id){
	if($sublevel_id){
		$sublevel_id = (!is_array($sublevel_id)) ? [$sublevel_id] : $sublevel_id;
		return DB::table('subjects')->whereIn('sublevel_id', $sublevel_id)->orderBy('id', 'ASC')->get();
	}
}
function getSubjectsById($sub_id){
	if($sub_id){
		return DB::table('subjects')->whereIn('id', $sub_id)->orderBy('id', 'ASC')->get();
	}
}
function getRegions(){
	return DB::table('region')->orderBy('id', 'ASC')->get();	
}
function getMRT($region_id=''){
	if($region_id)
	return DB::table('region_mrt')->where('region', $region_id)->orderBy('id', 'ASC')->get();	
}


function getSublevelName($Sublevel_id='') {
	  if($Sublevel_id){
		$Subl_id=  explode(",",$Sublevel_id);
		for($i=0;$i<count($Subl_id);$i++){
		$result = DB::table('sublevel')->where('id', $Subl_id[$i])->first();	
		
		}
		return $result->name;
	}
 } 
 
 function getSubjectName($Sub_name=''){
	  if($Sub_name){
	   $Sub_name=  explode(",",$Sub_name);
		   for($i=0;$i<count($Sub_name);$i++){
		 	
		    return $Sub_name[$i];
		  }
		 
	 }
 }


function sendWelcomeEmails($data){
	$data['sub'] = 'Welcome';
	$data['msg'] = '<p>Hello, '.Input::get('f_name').' '.Input::get('l_name').'</p>
						<p>Your account created by '.Session::get('user_name').' on Tutor</p>
						<p>Account detail as follows:</p>
						<p>Username: '.Input::get('email').'</p>
						<p>Password:  '.Input::get('password').'</p>';
	sendMail($data);
}
function getTutorSubjectRate($tutor_id='', $sublevel='', $subject=''){
	if($tutor_id != '' && $sublevel!='' && $subject != ''){
		$rate = DB::table('level_sub_rel')->where(['user_id' => $tutor_id, 'is_deleted' => 0]);
		$sublevel = strtolower(str_replace(' ', '', $sublevel));
		$subject = strtolower(str_replace(' ', '', $subject));
		$whereRaw = 'FIND_IN_SET("'.$sublevel.'", LOWER(REPLACE(sublevel_id, " ", ""))) and FIND_IN_SET("'.$subject.'", LOWER(REPLACE(sub_id, " ", "")))';
		$rate = $rate->whereRaw($whereRaw)->first();
		return isset($rate->rate) ? $rate->rate : 0;
	}
}
/*
* Count all unread msg received you
* 
* @return number of nuread msgs
* Ashvin Patel 27/Mar/2015
*/
function count_all_unread_msgs(){		
  $where = ['is_seeing' => 0, 'send_to' => Session::get('user_id'), 'is_deleted' => 0];
  $row = DB::table('chat')->where($where)->select(DB::raw('count(id) as n_msg'))->get();
  return $row[0]->n_msg;		
}

function getRegionsName($id=''){
	
	if($id)
	$result = DB::table('region')->where('id', $id)->get();
	if(!empty($result)){
	return $result[0]->region;
	}
	
}


function getUsers($name='', $att='', $id, $selected='')
{   $option = '';
	$result = DB::table('users')->where('type', $id)->get();

	foreach($result as $reidd)
	{
		
		if($reidd->id==$selected)
		{
			$select = 'selected';
		}
		else
		{
			$select = '';
		}
		$option .= '<option value="'.$reidd->id.'"'.$select.' >'.$reidd->name.'</option>';
	}
	echo '<select name="'.$name.'" '.$att.'>
		<option value="">Select</option>
		'.$option.'
	</select>';
	
}

function countClasses(){
		
}

?>