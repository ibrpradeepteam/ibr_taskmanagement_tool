Array
(
    [broker_f_name] => Ashvin
    [broker_m_name] => 
    [broker_l_name] => Patel
    [doing_business] => test
    [business_established] => 2013
    [state_licensed] => AK
    [insurance_license_nu] => 231231
    [my_business_license] => jkjk
    [hear_about_us] => Magazine
    [brokerage_sales_rep] => jkjkj
    [broker_phone_0] => 93
    [broker_phone_1] => 121
    [broker_phone_2] => 1321
    [broker_phone_3] => 2121
    [ph_type] => Array
        (
            [0] => Home
            [1] => Home
        )

    [ph_countr_code] => Array
        (
            [0] => 229
            [1] => 32
        )

    [ph_phone_0] => Array
        (
            [0] => 123
            [1] => 213
        )

    [ph_phone_1] => Array
        (
            [0] => 132
            [1] => 545
        )

    [ph_phone_2] => Array
        (
            [0] => 1212
            [1] => 567
        )

    [ph_ext] => Array
        (
            [0] => 12312
            [1] => 786
        )

    [ph_hour] => Array
        (
            [0] => 13212
            [1] => 6546
        )

    [broker_fax_0] => 229
    [broker_fax_1] => 123
    [broker_fax_2] => 4
    [email] => test@test.com
    [url] => ibrinfotech.com
    [address] => hjhjhj
    [city] => hjhj
    [state] => CA
    [zip1] => 5465
    [zip2] => 5465
    [principal_partners] => 2
    [agencyprincipal] => Array
        (
            [0] => kjhkh
            [1] => hkjhj
        )

    [agencyprincipal_mn] => Array
        (
            [0] => jhj
            [1] => hjkhj
        )

    [agencyprincipal_ln] => Array
        (
            [0] => hj
            [1] => hjkh
        )

    [agencyprincipal_dob] => Array
        (
            [0] => 12/12/2014
            [1] => 12/12/2014
        )

    [agencyphone_0] => Array
        (
            [0] => 355
            [1] => 355
        )

    [agencyphone_1] => Array
        (
            [0] => 121
            [1] => 45
        )

    [agencyphone_2] => Array
        (
            [0] => 1213
            [1] => 1324
        )

    [agencyphone_3] => Array
        (
            [0] => 2132
            [1] => 545
        )

    [agencyid_type] => Array
        (
            [0] => Passport
            [1] => DMV ID
        )

    [agencyid_number] => Array
        (
            [0] => 3333132123
            [1] => 64564
        )

    [licenses_partner_has] => Array
        (
            [0] => 1
            [1] => 2
        )

    [insurance_licence_1] => Array
        (
            [0] => 1231231
        )

    [insurance_license_state_1] => Array
        (
            [0] => CA
        )

    [insurance_licence_2] => Array
        (
            [0] => 7879845
            [1] => 87854
        )

    [insurance_license_state_2] => Array
        (
            [0] => CA
            [1] => CA
        )

    [agency_offices_you_have] => 1
    [agency_off_phone_0] => Array
        (
            [0] => 501
        )

    [agency_off_phone_1] => Array
        (
            [0] => 121
        )

    [agency_off_phone_2] => Array
        (
            [0] => 1231
        )

    [agency_off_phone_3] => Array
        (
            [0] => 1321
        )

    [agency_off_fax_0] => Array
        (
            [0] => 93
        )

    [agency_off_fax_1] => Array
        (
            [0] => 454
        )

    [agency_off_fax_2] => Array
        (
            [0] => 4564
        )

    [agency_off_email] => Array
        (
            [0] => test@test.com
        )

    [agency_off_url] => Array
        (
            [0] => inrinfotech.com
        )

    [agency_off_email1] => Array
        (
            [0] => test@test.com
        )

    [agency_off_address] => Array
        (
            [0] => hjhjhj
        )

    [agency_off_city] => Array
        (
            [0] => jhj
        )

    [agency_off_state] => Array
        (
            [0] => CO
        )

    [agency_off_zip] => Array
        (
            [0] => 12123
        )

    [agency_off_zip1] => Array
        (
            [0] => 1231
        )

    [more_office_submitting] => Array
        (
            [0] => yes
        )

    [license_producer] => Array
        (
            [0] => 2
        )

    [agency_off_title_1] => Array
        (
            [0] => Mr
            [1] => Mrs
        )

    [agency_off_fname_1] => Array
        (
            [0] => ghf
            [1] => ghf
        )

    [agency_off_mname_1] => Array
        (
            [0] => gfh
            [1] => gfh
        )

    [agency_off_lname_1] => Array
        (
            [0] => hgfh
            [1] => hgfh
        )

    [prod_license] => 0
    [num_state_license] => 1
    [p_license_state] => Array
        (
            [0] => CA
        )

    [p_license_type] => Array
        (
            [0] => Accident and Health Agent
            [1] => Administrator
            [2] => Bail Agent
        )

    [p_license_no] => Array
        (
            [0] => 121212
        )

    [p_license_issued] => Array
        (
            [0] => 12/12/2014
        )

    [p_license_expiration] => Array
        (
            [0] => 12/12/2014
        )

    [p_license_npn_no] => Array
        (
            [0] => 12121
        )

    [p_add_license_states] => Array
        (
            [0] => 1212
        )

    [accounting_name] => Array
        (
            [0] => hjhjh
            [1] => hjh
            [2] => hkjh
        )

    [accounting_middlename] => Array
        (
            [0] => hjhj
            [1] => jhjh
            [2] => oioi
        )

    [accounting_lastname] => Array
        (
            [0] => hjhj
            [1] => hjh
            [2] => jkjk
        )

    [accounting_phone_0] => Array
        (
            [0] => 93
            [1] => 93
            [2] => 213
        )

    [accounting_phone_1] => Array
        (
            [0] => 121
            [1] => 121
            [2] => 132
        )

    [accounting_phone_2] => Array
        (
            [0] => 2121
            [1] => 1321
            [2] => 212
        )

    [accounting_phone_3] => Array
        (
            [0] => 2112
            [1] => 12
            [2] => 12
        )

    [accounting_email] => Array
        (
            [0] => test@test.com
            [1] => tewst@test.com
            [2] => test@test.com
        )

    [accounting_partners] => 2
    [yes_count_for_copm_sec] => 0
    [judgement_demanded] => 0
    [judgement_year_declared] => 2014
    [judgement_court] => jhjkh
    [judgement_State] => hjkh
    [judgement_casenumber] => hjhj
    [judgement_details] => jhjhjhj
    [judgement_termination] => 0
    [termination_year] => 2015
    [termination_date] => 10
    [termination_term_details] => nkn
    [premium] => 0
    [premium_Organition] => hjghjg
    [premium_date] => ghgh
    [premium_details] => ghjghj
    [arbitration] => 0
    [arbitration_caseno] => 451545
    [arbitration_state] => jkjk
    [arbitration_country] => jkjk
    [arbitration_details] => jkjkjk
    [crime] => 0
    [crime_caseno] => 123124
    [crime_state] => 121254
    [crime_country] => 1214
    [crime_details] => jhjhjhj
    [bus_name] => 0
    [bank_name] => hjhj
    [bus_state] => hjh
    [bus_country] => hjghg
    [bus_name_details] => jhghg
    [license_incident] => 0
    [incident_case_number] => hgj
    [incident_state] => jhj
    [incident_number] => hjhj
    [license_incident_details] => hjhj
    [misconduct] => 0
    [misconduct_caseno] => 1321
    [misconduct_state] => jhjh
    [misconduct_no] => 121
    [misconduct_details] => kjkjkjkj
    [arrowhead_business] => 0
    [business_personname] => jhjh
    [business_agencyname] => hjh
    [business_date] => 10
    [arrowhead_business_details] => jhjhjh
    [other_business] => 0
    [other_business_personname] => ghjgh
    [other_business_type] => jhghjg
    [other_business_details] => ghgh
    [agency_license] => 0
    [num_agency_license] => 2
    [agency_license_state] => Array
        (
            [0] => CA
            [1] => CA
        )

    [agency_license_type] => Array
        (
            [0] => Administrator
            [1] => Bail Permittee
        )

    [agency_license_no] => Array
        (
            [0] => 121213
            [1] => 5454587878
        )

    [agency_npn_no] => Array
        (
            [0] => jhjh
            [1] => jkjkjkj
        )

    [agency_license_states] => Array
        (
            [0] => jhjhj, hjkh, hkjhkj, jhkjk
            [1] => jk, jkj, jkj, hjkh, hjh
        )

    [what_eo_company] => jhjhjh
    [eo_policyno] => 46545HHH-12
    [occurence_limit] => $ 0.12
    [occurence_deductible] => $ 12.12
    [occurence_effective_date] => 12/12/2014
    [occurence_expiration_date] => 12/12/2014
    [aggregate_limit] => $ 2.12
    [aggregate_deductible] => $ 0.12
    [eo_broker_name] => hjhj
    [eo_broker_name_m] => hjhj
    [eo_broker_name_l] => hjhj
    [eo_broker_phone_0] => 355
    [eo_broker_phone_1] => 121
    [eo_broker_phone_2] => 1212
    [eo_broker_phone_ex] => 212
    [eo_broker_fax_0] => 93
    [eo_broker_fax_1] => 121
    [eo_broker_fax_2] => 21
    [eo_broker_email] => test@test.com
    [eo_agencyname] => hjhj
    [eo_broker_url] => ibrinfotech.com
    [eo_broker_address] => ghjg
    [eo_broker_city] => hghg
    [eo_broker_state] => CO
    [eo_broker_zip1] => 2123
    [eo_broker_zip2] => 1212
    [broker_specialization] => 0
    [broker_specialization_details] => hjjkhjh
    [how_bank_reference] => 1
    [bank_ref_name] => Array
        (
            [0] => ggh
        )

    [bank_ref_contact_name] => Array
        (
            [0] => ghgh
        )

    [bank_ref_trust_ac_no] => Array
        (
            [0] => 121312
        )

    [bank_ref_phone_1] => Array
        (
            [0] => 93
        )

    [bank_ref_phone_2] => Array
        (
            [0] => 121
        )

    [bank_ref_phone_3] => Array
        (
            [0] => 121
        )

    [bank_ref_phone_4] => Array
        (
            [0] => 1212
        )

    [bank_ref_address] => Array
        (
            [0] => ghghgh
        )

    [bank_ref_city] => Array
        (
            [0] => ghg
        )

    [bank_ref_state] => Array
        (
            [0] => CA
        )

    [bank_ref_zip] => Array
        (
            [0] => 12122
        )

    [bank_ref_zip1] => Array
        (
            [0] => 121
        )

    [broker_info_date] => 12/12/2014
    [broker_info_initials] => Agree
    [ack_agree1] => Agree
    [authorized_contact] => 1
    [auth_contact_fname] => Array
        (
            [0] => jghjh
        )

    [auth_contact_mname] => Array
        (
            [0] => jhj
        )

    [auth_contact_lname] => Array
        (
            [0] => jhjh
        )

    [auth_contact_title] => Array
        (
            [0] => jhj
        )

    [auth_contact_tel_0] => Array
        (
            [0] => 93
        )

    [auth_contact_tel_1] => Array
        (
            [0] => 121
        )

    [auth_contact_tel_2] => Array
        (
            [0] => 2121
        )

    [auth_contact_tel_ex] => Array
        (
            [0] => 123
        )

    [auth_contact_fax_0] => Array
        (
            [0] => 93
        )

    [auth_contact_fax_1] => Array
        (
            [0] => 121
        )

    [auth_contact_fax_2] => Array
        (
            [0] => 121
        )

    [auth_contact_email] => Array
        (
            [0] => test@test.com
        )

    [checked_yes3] => Agree
    [agreement3_brokername] => jhhjh
    [agreement3_brokername_middle] => jhj
    [agreement3_brokername_last] => hjhj
    [agreement3_title] => hjhj
    [agreement3_org] => hjh
    [agreement3_org_liscense] => hjhj
    [chakedAgree_change7] => Agree
    [comment] => fgfdgfd
    [Val_14_name] => name
    [form11_name] => fdgd
    [form11_name_m] => fdg
    [form11_name_l] => gfdg
    [form11_title] => gfdgd
    [remarks] => fdgdgfdgd
    [broker_phone] => 93-121-1321-2121
    [broker_fax] => 229-123-4
    [zip_code] => 5465-5465
    [image] => 
    [date_created] => 2014-12-10 11:01:22
)