@include('includes.header')
@include('includes.left-sidebar')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Task Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->                   
    <!-- top row -->
    <div class="row">
        <div class="col-xs-12 connectedSortable">
            
        </div><!-- /.col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <div class="row" ng-app="project">                       
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-12" style="min-height:500px;">
            <!-- Map box --><script> var projects = '<?= $projects?>'; </script>
            <div class="box box-primary" style="min-height:500px;" >
            	<div class="col-md-12 top5">
                	<a href="javascript:void(0);" class="btn btn-primary add_project">Add Project</a>
                    <div class="top10 col-xs-12">
                       <table class="table table-bordered table-hover dataTable " id="example2">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($projects as $project)
                            <tr>
                                <td>{{$project['name']}}</td>
                                <td>{{$project['description']}}</td>
                                <td>{{$project['status']}}</td>  
                                <td><a href="javascript:void(0);" id="{{$project['id']}}" class=" btn btn-primary edit_project">View</a>
                                    <a href="javascript:void(0);" id="{{$project['id']}}" class=" btn btn-danger delete_project">Delete</a>
                                </td>                   
                            </tr>
                            @endforeach
                        </tbody>
                       </table>
                   </div>
               </div>
            </div>
        </section>
   </div>
<!-- /.box -->    
</section><!-- /.content -->   
<!--<form action="{{url()}}/set_value" method="post">
	<input type="hidden" name="_token" id="token">
    <input type="submit" value="submit"> 
</form>-->                  
@include('includes.footer')

<script type="text/javascript">
  $(function() {
	  //$('#token').val(csrf_token);
	  $("#example1").dataTable();
	  $('#example2').dataTable({
		  "bPaginate": true,
		  "bLengthChange": true,
		  "bFilter": true,
		  "bSort": true,
		  "bInfo": true,
		  "bAutoWidth": false
	  });
  });
</script><script>
var myApp = angular.module('project',[]);
/*function project_list($http, $scope, $rootScope, $window) { 
    $scope.projects = $window.projects;
	$scope.init = function(projects){
		//console.log($.parseJSON($scope.projects));	
		$scope.projects = $.parseJSON($scope.projects);
	}
}*/
</script>
