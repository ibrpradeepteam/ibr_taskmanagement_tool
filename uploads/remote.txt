We need control our software license with remote server. So when they purchase our software and merchant confirm their orders, license active for them automatically. When they ask refund license disable automatically.
Software will connect HTTP request to remote server and server response license is valid or not.

Our merchant is  www.clickbank.com and they have IPN system as below:
https://support.clickbank.com/entries/22803622-Instant-Notification-Service

IPN sends different info for each order but we need some of them.
When one client complete purchase in Clickbank, clickbank sends some information from this order to vendor server. You can find details of this info in above link.
I need you read some parts of this info and store in one DB(mysql) in Admin server. We need only save "ClickBank receipt" and "fullname"  and "email" of client.
"ClickBank receipt" is unique number and we want control all license with this number. 



License management panel:

We have two parts:
1- Admin Panel
2- Client Panel

Admin Panel:
In admin Panel, admin must able search for different "ClickBank receipt" and see details for each, same �email�, �Full name�, and one field �Account Number� and validity license field. All this 5 fields must be shows in one table and admin must be able to edit/modify fields.
�Account Number� will be add by client and from client panel.

Client Panel:
When client complete order, clickbank sends �ClickBank receipt� for them. Client must be able login with this number to his panel. After login he will see fields same ClickBank receipt, �Email�, �FullName�, and one empty filed �Account number�. Client will add his �Account number� and save this.
So client every time can login to his panel and change his �Account Number� and this data must update in remote server DB.

System how works:
Client add "ClickBank receipt" in Software setting. EA read this "ClickBank receipt" and send by HTTP request to Admin server and server read this "ClickBank receipt" and search in DB for this "ClickBank receipt". Finds the "account number" that registered for this "ClickBank receipt" and return to Software. Software will compare this account number from server with MT4 account number. If both values will be same license is valid and client can use software.
If not valid he will get error.

For example 
account number is: 12345
ClickBank receipt: 1414
Remote server page: www.rrr.com/remote
Software send request same this:  www.rrr.com/remote?1414
Server read 1414 in DB and print registered �Account number� for this 1414 and show in one page. Software read this �Account Number�.
This response page can be blank and shows �Account Number� and if not any �account number� print nothing.

How to manage Refund:
When client refund, Clickbank send IPN for refund to server. Server must read this �ClickBank receipt� and disable that.

All code must be in php and DataBase is mysql too.
If you have any question please ask me. 



