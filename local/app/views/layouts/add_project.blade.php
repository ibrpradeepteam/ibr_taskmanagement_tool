@include('includes.head')
<div class="row">
	<div class="col-xs-12">
    	<form action="" method="post" enctype="multipart/form-data">
    	<div class="row">
        	<div class="col-xs-8">
            	<label>Name</label>
                <input type="text" name="name" class="form-control input-sm" value="{{isset($project->name) ? $project->name : ''}}">
                <input type="hidden" name="id" class="form-control input-sm" value="{{isset($project->id) ? $project->id : ''}}">
            </div>
        </div>
        <div class="row top5">
        	<div class="col-xs-12">
            	<label>Description</label>
                <textarea  name="description" rows="3" class="form-control input-sm">{{isset($project->description) ? $project->description : ''}}</textarea>
            </div>
        </div>
		
		<div class="row top5">
        	<div class="col-xs-2">
            	<label>Status</label>
                <input  name="status"  class="form-control input-sm" value="{{isset($project->status) ? $project->status : ''}}" />
            </div>
       
        	<div class="col-xs-3">
            	<label>Project Link</label>
                <input  name="link" class="form-control input-sm" value="{{isset($project->link) ? $project->link : ''}}" />
            </div>
       
        	<div class="col-xs-2">
            	<label>Tags</label>
                <input  name="tags" class="form-control input-sm" value="{{isset($project->tag) ? $project->tag : ''}}" />
            </div>
            <div class="col-xs-2">
            	<label>File Upload</label>
                <input  type="file" name="project_file[]" multiple  class="" value="{{isset($project->project_file) ? $project->project_file : ''}}" />
                <?php $file=!empty($project->project_file) ? explode(',',$project->project_file):'';
				  if(is_array($file)){
				  for($i=0; count($file) > $i; $i++){	?>                
                <a href="{{url()}}/uploads/<?php echo $file[$i]; ?>" download target="_new"><?php echo $file[$i]; ?></a><br/>
                <?php } } ?>
            </div>
        </div>
		
		<fieldset>
			<legend>Peoples</legend>
			<div class="row top5">
				<div class="col-xs-2">
					<label>Manager</label>
                 <?php   $manager= isset($project->manager) ? $project->manager : '';?>
                    {{getUsers('manager', 'class="form-control input-sm"', 1, $manager)}}
					<!--<input  name="manager"  class="form-control input-sm" value="{{isset($project->manager) ? $project->manager : ''}}" />-->
				</div>
				
				<div class="col-xs-2">
					<label>Developer</label>
                     <?php   $developer= isset($project->developer) ? $project->developer : '';?>
                    {{getUsers('developer', 'class="form-control input-sm"', 2, $developer)}}
					<!--<input  name="developer"  class="form-control input-sm" value="{{isset($project->developer) ? $project->developer : ''}}" />-->
				</div>
			
			</div>
				
			
			</div>
			
		</fieldset>
		<br/>
		
        <div class="row top5 " ng-app ng-controller="RePeat">
        	<div class="input form-group col-xs-3 top10 left15">
        		<a href="#" ng-click="add_row();" class="btn btn-success  add_row" >Add Credentials</a>
            </div>
        	<div class="col-xs-12" ng-repeat = "data in datarow">
           
            	<div class="input form-group col-xs-2">
                	<label >Link</label>
            		<input type="text"  name="cr_link[]" class="form-control input-sm" ng-model="data.cr_link" value="">
                </div>
                <div class="input form-group col-xs-2">
                	<label>Username</label>
                	 <input type="text"  name="username[]" class="form-control input-sm"  ng-model="data.username" value="">
                </div>
                <div class="input form-group col-xs-2">
                	 <label>Password</label>
                	 <input type="text"  name="password[]" class="form-control input-sm"  ng-model="data.password" value="">
                </div>
				<div class="input form-group col-xs-2">
                	<label >Type</label>
            		<input type="text"  name="type[]" class="form-control input-sm" ng-model="data.type" value="">
                </div>
                <div class="input form-group col-xs-2">
                	 <label>Remarks</label>
                	 <input type="text"  name="remark[]" class="form-control input-sm"  ng-model="data.remark" value="">
                </div>
                 <div class="input form-group col-xs-2">
                	  
                     
                     <input type="button" value="X" ng-click="delet_row($index);" class="sm-btn btn-danger add_row" />
                </div>
              
            </div>
        </div>
        
        <div class="row top10">
        	<div class="col-xs-12 left15">
            	<input type="hidden" name="_token" value="{{csrf_token()}}">
            	<input type="submit" name="submit" value="Submit" class="btn btn-primary">
            </div>
        </div>
        
        </form>
    </div>
</div>
@include('includes.footer')

<script>
function RePeat($http, $scope, $rootScope, $window) {
 		//var levels = $.parseJSON($window.levels_0);
		
		 var json = '<?php echo isset($details) ? json_encode($details): '' ;?>';
		 if(json != ''){
		 	$scope.datarow = $.parseJSON(json);
		  }else{
			 $scope.datarow = [];
		 	 $scope.datarow.push(
					{ column1: 'row #' + $scope.datarow.length}
				);
		  }
		 
		 $scope.add_row = function() {
			$scope.datarow.push(
					{ column1: 'row #' + $scope.datarow.length}
				);
		};
			
		  $scope.delet_row = function(index) {
				  var retVal = confirm("Are you sure you want to delete this row");
				  if (retVal == true) {
					$scope.datarow.splice(index, 1);
				  }  
		  }
 

}


</script>